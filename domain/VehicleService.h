//
// Created by Adamczyk Piotr on 29.11.2019.
//

#ifndef WSEICARRENTAL_VEHICLESERVICE_H
#define WSEICARRENTAL_VEHICLESERVICE_H


#include <string>
#include <vector>
#include <memory>
#include "VehicleDto.h"
#include "IVehicleRepository.h"
#include "Observer.h"


class VehicleService : public Observable<> {
public:
    using Observer = Observable<>::constIt;
private:
    std::shared_ptr<IVehicleRepository> vehicleRepository;

public:
    explicit VehicleService(std::shared_ptr<IVehicleRepository> &vehicleRepository);

    int AddVehicle(const std::string &plate, const std::string &type, const std::string &description,
                   const std::string &price);

    bool DeleteVehicle(int id);

    VehicleDto GetVehicle(int id);

    std::optional<Vehicle> GetVehicled(int id);

    std::vector<VehicleDto> GetVehicles();


};


#endif //WSEICARRENTAL_VEHICLESERVICE_H

