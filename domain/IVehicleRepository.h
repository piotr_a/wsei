//
// Created by Adamczyk Piotr on 29.11.2019.
//

#ifndef WSEICARRENTAL_IVEHICLEREPOSITORY_H
#define WSEICARRENTAL_IVEHICLEREPOSITORY_H

#include <vector>
#include <optional>
#include "Vehicle.h"

class IVehicleRepository {

public:
    virtual ~IVehicleRepository() = default;

    virtual int AddVehicle(Vehicle &vehicle) = 0;

    virtual bool DeleteVehicle(int id) = 0;

    virtual std::optional<Vehicle> GetVehicle(int id) = 0;

    virtual std::vector<Vehicle> GetVehicles() = 0;
};

#endif //WSEICARRENTAL_IVEHICLEREPOSITORY_H
