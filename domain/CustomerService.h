//
// Created by Adamczyk Piotr on 29.11.2019.
//

#ifndef WSEICARRENTAL_CUSTOMERSERVICE_H
#define WSEICARRENTAL_CUSTOMERSERVICE_H


#include <string>
#include <vector>
#include <memory>
#include "ICustomerRepository.h"
#include "CustomerDto.h"
#include "Observer.h"

class CustomerService : public Observable<> {
public:
    using Observer = Observable<>::constIt;
private:
    std::shared_ptr<ICustomerRepository> customerRepository;
public:
    explicit CustomerService(std::shared_ptr<ICustomerRepository> &customerRepository);

    int AddCustomer(const std::string &name, const std::string &surname, const std::string &address,
                    const std::string &phoneNumber);

    bool DeleteCustomer(int id);

    CustomerDto GetCustomer(int id);

    std::vector<CustomerDto> GetCustomers();

};


#endif //WSEICARRENTAL_CUSTOMERSERVICE_H
