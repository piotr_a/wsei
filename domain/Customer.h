//
// Created by Adamczyk Piotr on 26.11.2019.
//

#ifndef WSEICARRENTAL_CUSTOMER_H
#define WSEICARRENTAL_CUSTOMER_H


#include <string>
#include <vector>
#include "Rent.h"

class Customer {
public:
    int Id;
    std::string Name;
    std::string Surname;
    std::string Address;
    std::string PhoneNumber;
};


#endif //WSEICARRENTAL_CUSTOMER_H
