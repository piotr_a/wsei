//
// Created by Adamczyk Piotr on 26.11.2019.
//

#include <sstream>
#include <iomanip>
#include "Rent.h"

std::string Rent::CalculatePay() {
    auto now = std::time(nullptr);
    auto diff = std::difftime(now, rentFrom);

    int days = static_cast<int>((diff / (24 * 60 * 60))) + 1;
    int pay = days * price;
    std::stringstream ss;
    ss << pay / 100 << "," << std::setw(2) << std::setfill('0') << pay % 100;
    return ss.str();
}
