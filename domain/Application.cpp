//
// Created by Adamczyk Piotr on 05.12.2019.
//

#include "Application.h"


Application::Application()
        : onDashboardStateChange(nullptr),
          onRentCarStateChange(nullptr),
          onReturnCarStateChange(nullptr),
          onVehiclesStateChange(nullptr) {
    applicationState = &dashboardState;
}

void Application::setNextStage(NextStage &nextStage) {
    if (nextStage) {
        applicationState = *nextStage;
        std::visit(overload{
                [&](DashboardState *state) { onDashboardStateChange(); },
                [&](RentVehicleState *state) { onRentCarStateChange(); },
                [&](ReturnVehicleState *state) { onReturnCarStateChange(); },
                [&](VehiclesState *state) { onVehiclesStateChange(); },
        }, applicationState);
    }
}


void Application::handle(const Event::RentSelect &event) {
    NextStage nextStage = std::visit(overload{
            [&](DashboardState *state) -> NextStage { return &rentVehicleState; },
            [&](RentVehicleState *state) -> NextStage { return std::nullopt; },
            [&](ReturnVehicleState *state) -> NextStage { return &rentVehicleState; },
            [&](VehiclesState *state) -> NextStage { return &rentVehicleState; },
    }, applicationState);
    setNextStage(nextStage);
}

void Application::handle(const Event::ReturnVehicleSelect &event) {
    NextStage nextStage = std::visit(overload{
            [&](DashboardState *state) -> NextStage { return &returnVehicleState; },
            [&](RentVehicleState *state) -> NextStage { return &returnVehicleState; },
            [&](ReturnVehicleState *state) -> NextStage { return std::nullopt; },
            [&](VehiclesState *state) -> NextStage { return &returnVehicleState; },
    }, applicationState);
    setNextStage(nextStage);
}

void Application::handle(const Event::Cancel &event) {
    NextStage nextStage = std::visit(overload{
            [&](DashboardState *state) -> NextStage { return std::nullopt; },
            [&](RentVehicleState *state) -> NextStage { return &dashboardState; },
            [&](ReturnVehicleState *state) -> NextStage { return &dashboardState; },
            [&](VehiclesState *state) -> NextStage { return &dashboardState; },
    }, applicationState);
    setNextStage(nextStage);
}

void Application::handle(const Event::VehiclesSelect &event) {
    NextStage nextStage = std::visit(overload{
            [&](DashboardState *state) -> NextStage { return &vehiclesState; },
            [&](RentVehicleState *state) -> NextStage { return &vehiclesState; },
            [&](ReturnVehicleState *state) -> NextStage { return &vehiclesState; },
            [&](VehiclesState *state) -> NextStage { return std::nullopt; },
    }, applicationState);
    setNextStage(nextStage);
}

void Application::setOnDashboardStateChange(const std::function<void(void)> &onDashboardStateChange) {
    Application::onDashboardStateChange = onDashboardStateChange;
}

void Application::setOnRentCarStateChange(const std::function<void(void)> &onRentCarStateChange) {
    Application::onRentCarStateChange = onRentCarStateChange;
}

void Application::setOnReturnCarStateChange(const std::function<void(void)> &onReturnCarStateChange) {
    Application::onReturnCarStateChange = onReturnCarStateChange;
}

void Application::setOnVehiclesStateChange(const std::function<void(void)> &onVehiclesStateChange) {
    Application::onVehiclesStateChange = onVehiclesStateChange;
}

