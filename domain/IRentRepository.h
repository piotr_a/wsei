//
// Created by Adamczyk Piotr on 18.12.2019.
//

#ifndef WSEICARRENTAL_IRENTREPOSITORY_H
#define WSEICARRENTAL_IRENTREPOSITORY_H

#include <optional>
#include <vector>
#include "Rent.h"

class IRentRepository {

public:
    virtual ~IRentRepository() = default;

    virtual int AddRent(Rent &rent) = 0;

    virtual bool DeleteRent(int id) = 0;

    virtual std::optional<Rent> GetRent(int id) = 0;

    virtual std::vector<Rent> GetRents() = 0;
};

#endif //WSEICARRENTAL_IRENTREPOSITORY_H
