//
// Created by Adamczyk Piotr on 26.11.2019.
//

#ifndef WSEICARRENTAL_RENT_H
#define WSEICARRENTAL_RENT_H


#include <string>
#include <ctime>

class Rent {
public:
    int Id{};
    int vehicleId{};
    int customerId{};
    bool returned{};
    int price{};
    std::time_t rentFrom{};
    std::time_t rentTo{};

    [[nodiscard]] std::string CalculatePay();

};


#endif //WSEICARRENTAL_RENT_H
