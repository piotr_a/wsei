//
// Created by Adamczyk Piotr on 18.12.2019.
//

#ifndef WSEICARRENTAL_RENTSERVICE_H
#define WSEICARRENTAL_RENTSERVICE_H


#include <memory>
#include <vector>
#include "IRentRepository.h"
#include "RentDto.h"
#include "Observer.h"
#include "VehicleService.h"
#include "CustomerService.h"

class RentService : public Observable<> {
public:
    using Observer = Observable<>::constIt;
private:
    std::shared_ptr<IRentRepository> rentRepository;
    std::shared_ptr<VehicleService> vehicleService;
    std::shared_ptr<CustomerService> customerService;

    [[nodiscard]] std::string getTimeString(const Rent &rent) const;

public:
    explicit RentService(std::shared_ptr<IRentRepository> &rentRepository,
                         std::shared_ptr<VehicleService> &vehicleService,
                         std::shared_ptr<CustomerService> &customerService);

    int AddRent(int vehicleId, int customerId);

    bool ReturnRent(int id);

    std::optional<Rent> GetRent(int id);

    std::vector<RentDto> GetRents();

    std::vector<RentDto> GetActiveRents();

    bool IsVehicleRented(int id);


};


#endif //WSEICARRENTAL_RENTSERVICE_H
