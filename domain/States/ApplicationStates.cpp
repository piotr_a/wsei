//
// Created by Adamczyk Piotr on 02.01.2020.
//

#include "ApplicationStates.h"


void VehiclesState::handle(const Event::Vehicles::Add &event) {
    add = true;
    onVehiclesAddStateChange(add);
}

void VehiclesState::handle(const Event::Vehicles::AddAdd &event) {
    add = false;
    onVehiclesAddStateChange(add);
}

void VehiclesState::handle(const Event::Vehicles::AddCancel &event) {
    add = false;
    onVehiclesAddStateChange(add);
}

void RentVehicleState::handle(const Event::Rent::SelectVehicle &event) {
    vehicleId = event.id;
    onVehicleChange(vehicleId != 0);
}

void RentVehicleState::handle(const Event::Rent::SelectCustomer &event) {
    customerId = event.id;
    onCustomerChange(customerId != 0);
}

void RentVehicleState::handle(const Event::Rent::AddCustomer &event) {
    onCustomerAdd();
}

void RentVehicleState::handle(const Event::Rent::Cancel &event) {
    vehicleId = 0;
    customerId = 0;
    onClear();
}
