//
// Created by Adamczyk Piotr on 02.01.2020.
//

#ifndef WSEICARRENTAL_APPLICATIONSTATES_H
#define WSEICARRENTAL_APPLICATIONSTATES_H


#include <functional>
#include "../Events/VehiclesEvents.h"
#include "../Events/RentEvents.h"


struct DashboardState {
};

struct RentVehicleState {
    int vehicleId;
    int customerId;

    void handle(const Event::Rent::SelectVehicle &event);

    void handle(const Event::Rent::SelectCustomer &event);

    void handle(const Event::Rent::AddCustomer &event);

    void handle(const Event::Rent::Cancel &event);

    template<class Tevent>
    void handle(const Tevent &event) {}

    std::function<void(bool selected)> onVehicleChange;
    std::function<void(bool selected)> onCustomerChange;
    std::function<void(void)> onCustomerAdd;
    std::function<void(void)> onClear;
};

struct ReturnVehicleState {
};

struct VehiclesState {
    bool add;

    void handle(const Event::Vehicles::Add &event);

    void handle(const Event::Vehicles::AddAdd &event);

    void handle(const Event::Vehicles::AddCancel &event);

    template<class Tevent>
    void handle(const Tevent &event) {}

    std::function<void(bool addMode)> onVehiclesAddStateChange;
};


#endif //WSEICARRENTAL_APPLICATIONSTATES_H
