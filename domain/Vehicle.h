//
// Created by Adamczyk Piotr on 26.11.2019.
//

#ifndef WSEICARRENTAL_VEHICLE_H
#define WSEICARRENTAL_VEHICLE_H


#include <string>

class Vehicle {
public:
    int id;
    std::string plate;
    std::string type;
    std::string description;
    int price;

    [[nodiscard]]  std::string getPrice() const;

    void setPrice(const std::string &price);
};


#endif //WSEICARRENTAL_VEHICLE_H
