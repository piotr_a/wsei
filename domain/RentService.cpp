//
// Created by Adamczyk Piotr on 18.12.2019.
//

#include "RentService.h"
#include <algorithm>

RentService::RentService(std::shared_ptr<IRentRepository> &rentRepository,
                         std::shared_ptr<VehicleService> &vehicleService,
                         std::shared_ptr<CustomerService> &customerService)
        : rentRepository(rentRepository),
          vehicleService(vehicleService),
          customerService(customerService) {}

int RentService::AddRent(int vehicleId, int customerId) {
    Rent rent;
    rent.Id = 0;
    rent.vehicleId = vehicleId;
    rent.customerId = customerId;
    rent.returned = false;
    rent.price = vehicleService->GetVehicled(vehicleId)->price;
    rent.rentFrom = std::time(nullptr);
    bool added = rentRepository->AddRent(rent);
    if (added) Notify();
    return added;
}

bool RentService::ReturnRent(int id) {
    auto rent = rentRepository->GetRent(id);
    if (!rent) return false;
    rent->returned = true;
    rent->rentTo = std::time(nullptr);
    rentRepository->DeleteRent(id);
    rentRepository->AddRent(*rent);
    Notify();
    return true;
}

std::optional<Rent> RentService::GetRent(int id) {
    return rentRepository->GetRent(id);
}

std::vector<RentDto> RentService::GetRents() {
    auto rents = rentRepository->GetRents();
    std::vector<RentDto> vehicleDtos;
    vehicleDtos.reserve(rents.size());
    for (auto &&rent : rents) {
        auto customerDto = customerService->GetCustomer(rent.customerId);
        RentDto rentDto{rent.Id,
                        vehicleService->GetVehicle(rent.vehicleId).plate,
                        customerDto.Name,
                        customerDto.Surname,
                        getTimeString(rent)};
        vehicleDtos.push_back(rentDto);
    }
    return vehicleDtos;
}

std::vector<RentDto> RentService::GetActiveRents() {
    auto rents = rentRepository->GetRents();
    std::vector<RentDto> vehicleDtos;
    vehicleDtos.reserve(rents.size());

    for (auto &&rent : rents) {
        if (!rent.returned) {
            auto customerDto = customerService->GetCustomer(rent.customerId);
            RentDto rentDto{rent.Id,
                            vehicleService->GetVehicle(rent.vehicleId).plate,
                            customerDto.Name,
                            customerDto.Surname,
                            getTimeString(rent)};
            vehicleDtos.push_back(rentDto);
        }
    }
    return vehicleDtos;
}

bool RentService::IsVehicleRented(int id) {
    auto rents = rentRepository->GetRents();
    for (auto &&rent : rents) {
        if (!rent.returned) {
            if (rent.vehicleId == id) return true;
        }
    }
    return false;
}


std::string RentService::getTimeString(const Rent &rent) const {
    std::string time;
    time.resize(40);
    ctime_s(time.data(), 40, &rent.rentFrom);
    time.erase(std::remove_if(time.begin(), time.end(), [](unsigned char c) { return c == '\n'; }), time.end());
    return time;
}
