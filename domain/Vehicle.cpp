//
// Created by Adamczyk Piotr on 26.11.2019.
//


#include <iomanip>
#include "Vehicle.h"
#include <sstream>

std::string Vehicle::getPrice() const {
    std::stringstream ss;
    ss << price / 100 << "," << std::setw(2) << std::setfill('0') << price % 100;
    return ss.str();
}

void Vehicle::setPrice(const std::string &price) {
    double priceDouble = std::stod(price) * 100.00;
    Vehicle::price = static_cast<int>(priceDouble);
}
