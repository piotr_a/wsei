//
// Created by Adamczyk Piotr on 18.12.2019.
//

#ifndef WSEICARRENTAL_RENTDTO_H
#define WSEICARRENTAL_RENTDTO_H


#include <string>
#include <chrono>

class RentDto {
public:
    int id;
    std::string plate;
    std::string name;
    std::string surname;
    std::string rentFrom;
};


#endif //WSEICARRENTAL_RENTDTO_H
