//
// Created by Adamczyk Piotr on 04.01.2020.
//

#ifndef WSEICARRENTAL_SERVICES_H
#define WSEICARRENTAL_SERVICES_H

#include <memory>
#include "VehicleService.h"
#include "CustomerService.h"
#include "RentService.h"

class Services {
public:
    std::shared_ptr<VehicleService> Vehicle;
    std::shared_ptr<CustomerService> Customer;
    std::shared_ptr<RentService> Rent;
};

#endif //WSEICARRENTAL_SERVICES_H
