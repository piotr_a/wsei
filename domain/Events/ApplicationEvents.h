//
// Created by Adamczyk Piotr on 02.01.2020.
//

#ifndef WSEICARRENTAL_APPLICATIONEVENTS_H
#define WSEICARRENTAL_APPLICATIONEVENTS_H
namespace Event {
    struct RentSelect {
    };
    struct ReturnVehicleSelect {
    };
    struct Cancel {
    };
    struct VehiclesSelect {
    };
}
#endif //WSEICARRENTAL_APPLICATIONEVENTS_H
