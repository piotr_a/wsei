//
// Created by Adamczyk Piotr on 02.01.2020.
//

#ifndef WSEICARRENTAL_RENTEVENTS_H
#define WSEICARRENTAL_RENTEVENTS_H
namespace Event::Rent {
    struct SelectVehicle {
        int id;
    };
    struct SelectCustomer {
        int id;
    };
    struct AddCustomer {
    };
    struct Cancel {
    };
}

#endif //WSEICARRENTAL_RENTEVENTS_H
