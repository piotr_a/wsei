//
// Created by Adamczyk Piotr on 05.12.2019.
//

#ifndef WSEICARRENTAL_APPLICATION_H
#define WSEICARRENTAL_APPLICATION_H

#include <functional>
#include <variant>
#include <optional>
#include "Events/ApplicationEvents.h"
#include "States/ApplicationStates.h"

template<class... Ts>
struct overload : Ts ... {
    using Ts::operator()...;
};
template<class... Ts> overload(Ts...) -> overload<Ts...>;


class Application {
public:
    DashboardState dashboardState;
    RentVehicleState rentVehicleState;
    ReturnVehicleState returnVehicleState;
    VehiclesState vehiclesState;
private:
    using ApplicationState = std::variant<DashboardState *, RentVehicleState *, ReturnVehicleState *, VehiclesState *>;
    using NextStage = std::optional<ApplicationState>;
    ApplicationState applicationState;

    std::function<void(void)> onDashboardStateChange;
    std::function<void(void)> onRentCarStateChange;
    std::function<void(void)> onReturnCarStateChange;
    std::function<void(void)> onVehiclesStateChange;

    void setNextStage(NextStage &nextStage);

public:
    Application();

    void handle(const Event::RentSelect &event);

    void handle(const Event::ReturnVehicleSelect &event);

    void handle(const Event::Cancel &event);

    void handle(const Event::VehiclesSelect &event);

    template<class Tevent>
    void handle(const Tevent &event) {
        std::visit(overload{
                [&](DashboardState *state) {},
                [&](RentVehicleState *state) { state->handle(event); },
                [&](ReturnVehicleState *state) {},
                [&](VehiclesState *state) { state->handle(event); },
        }, applicationState);
    }

    void setOnDashboardStateChange(const std::function<void(void)> &onDashboardStateChange);

    void setOnRentCarStateChange(const std::function<void(void)> &onRentCarStateChange);

    void setOnReturnCarStateChange(const std::function<void(void)> &onReturnCarStateChange);

    void setOnVehiclesStateChange(const std::function<void(void)> &onVehiclesStateChange);
};


#endif //WSEICARRENTAL_APPLICATION_H