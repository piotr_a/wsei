//
// Created by Adamczyk Piotr on 03.12.2019.
//

#ifndef WSEICARRENTAL_VEHICLEDTO_H
#define WSEICARRENTAL_VEHICLEDTO_H


#include <string>

class VehicleDto {
public:
    int id;
    std::string plate;
    std::string type;
    std::string description;
    std::string price;
};


#endif //WSEICARRENTAL_VEHICLEDTO_H
