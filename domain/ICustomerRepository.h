//
// Created by Adamczyk Piotr on 06.12.2019.
//

#ifndef WSEICARRENTAL_ICUSTOMERREPOSITORY_H
#define WSEICARRENTAL_ICUSTOMERREPOSITORY_H


#include <vector>
#include <optional>
#include "Customer.h"

class ICustomerRepository {
public:
    virtual ~ICustomerRepository() = default;

    virtual int AddCustomer(Customer &rCustomer) = 0;

    virtual bool DeleteCustomer(int id) = 0;

    virtual std::optional<Customer> GetCustomer(int id) = 0;

    virtual std::vector<Customer> GetCustomers() = 0;
};


#endif //WSEICARRENTAL_ICUSTOMERREPOSITORY_H
