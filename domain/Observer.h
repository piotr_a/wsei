//
// Created by Adamczyk Piotr on 03.01.2020.
//

#ifndef WSEICARRENTAL_OBSERVER_H
#define WSEICARRENTAL_OBSERVER_H

#include <functional>
#include <list>

template<class... Args>
class Observable {
private:
    std::list<std::function<void(Args...)>> observers;


public:
    using constIt = typename std::list<std::function<void(Args...)>>::const_iterator;

    template<typename Observer>
    constIt Register(Observer &&observer) {
        observers.push_back(std::forward<Observer>(observer));
        return std::prev(observers.end());
    }

    void Remove(constIt iterator) {
        observers.erase(iterator);
    }

    void Notify(Args... args) {
        try {
            for (auto &observer : observers)
                observer(args...);
        }
        catch (...) {}
    }
};


#endif //WSEICARRENTAL_OBSERVER_H
