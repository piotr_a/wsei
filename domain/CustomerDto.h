//
// Created by Adamczyk Piotr on 06.12.2019.
//

#ifndef WSEICARRENTAL_CUSTOMERDTO_H
#define WSEICARRENTAL_CUSTOMERDTO_H


#include <string>

class CustomerDto {
public:
    int Id;
    std::string Name;
    std::string Surname;
    std::string Address;
    std::string PhoneNumber;
};


#endif //WSEICARRENTAL_CUSTOMERDTO_H
