//
// Created by Adamczyk Piotr on 29.11.2019.
//

#include "CustomerService.h"

CustomerService::CustomerService(std::shared_ptr<ICustomerRepository> &customerRepository) : customerRepository(
        customerRepository) {}

int CustomerService::AddCustomer(const std::string &name, const std::string &surname, const std::string &address,
                                 const std::string &phoneNumber) {
    Customer customer;
    customer.Id = 0;
    customer.Name = name;
    customer.Surname = surname;
    customer.Address = address;
    customer.PhoneNumber = phoneNumber;
    int id = customerRepository->AddCustomer(customer);
    if (id != 0) Notify();
    return id;
}

bool CustomerService::DeleteCustomer(int id) {
    bool deleted = customerRepository->DeleteCustomer(id);
    if (deleted) Notify();
    return deleted;
}

CustomerDto CustomerService::GetCustomer(int id) {
    auto customer = customerRepository->GetCustomer(id);
    if (!customer)return {0, "", "", "", ""};
    return {customer->Id, customer->Name, customer->Surname, customer->Address, customer->PhoneNumber};
}

std::vector<CustomerDto> CustomerService::GetCustomers() {
    auto customers = customerRepository->GetCustomers();
    std::vector<CustomerDto> customerDtos;
    customerDtos.reserve(customers.size());
    for (auto &&customer : customers) {
        CustomerDto customerDto{customer.Id, customer.Name, customer.Surname, customer.Address, customer.PhoneNumber};
        customerDtos.push_back(customerDto);
    }
    return customerDtos;
}
