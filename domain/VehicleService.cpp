//
// Created by Adamczyk Piotr on 29.11.2019.
//

#include "VehicleService.h"

VehicleService::VehicleService(std::shared_ptr<IVehicleRepository> &vehicleRepository) : vehicleRepository(
        vehicleRepository) {}

int VehicleService::AddVehicle(const std::string &plate, const std::string &type, const std::string &description,
                               const std::string &price) {
    Vehicle vehicle;
    vehicle.id = 0;
    vehicle.plate = plate;
    vehicle.type = type;
    vehicle.description = description;
    vehicle.setPrice(price);
    int id = vehicleRepository->AddVehicle(vehicle);
    if (id != 0) Notify();
    return id;
}

bool VehicleService::DeleteVehicle(int id) {
    bool deleted = vehicleRepository->DeleteVehicle(id);
    if (deleted) Notify();
    return deleted;
}

VehicleDto VehicleService::GetVehicle(int id) {
    auto vehicle = vehicleRepository->GetVehicle(id);
    if (!vehicle)return {0, "", ""};
    return {vehicle->id, vehicle->plate, vehicle->type, vehicle->description, vehicle->getPrice()};
}

std::optional<Vehicle> VehicleService::GetVehicled(int id) {
    return vehicleRepository->GetVehicle(id);
}

std::vector<VehicleDto> VehicleService::GetVehicles() {
    auto vehicles = vehicleRepository->GetVehicles();
    std::vector<VehicleDto> vehicleDtos;
    vehicleDtos.reserve(vehicles.size());
    for (auto &&vehicle : vehicles) {
        VehicleDto vehicleDto{vehicle.id, vehicle.plate, vehicle.type, vehicle.description, vehicle.getPrice()};
        vehicleDtos.push_back(vehicleDto);
    }
    return vehicleDtos;
}
