


#include "gui/_gtk/GtkViewBuilder.h"
#include "gui/ApplicationPresenter.h"
#include "data/IDataBuilder.h"
#include "data/oracle/OracleDataBuilder.h"
#include "data/inMemory/InMemoryDataBuilder.h"


int main(int argc, char *argv[]) {

    enum {
        inMemory, oracle
    } data;
    data = inMemory;
    std::string user = "wsei";
    std::string password = "wsei123";
    std::string database = "127.0.0.1";

    for (int i = 0; i < argc - 1; ++i) {
        if (strcmp(argv[i], "-c") == 0) {
            if (strcmp(argv[i + 1], "oracle") == 0) {
                data = oracle;
            }
        } else if (strcmp(argv[i], "-u") == 0) {
            user = argv[i + 1];
        } else if (strcmp(argv[i], "-p") == 0) {
            password = argv[i + 1];
        } else if (strcmp(argv[i], "-d") == 0) {
            database = argv[i + 1];
        }

    }

    std::shared_ptr<IDataBuilder> dataBuilder;
    switch (data) {
        case inMemory:
            dataBuilder = std::make_shared<Data::InMemory::InMemoryDataBuilder>();
            break;
        case oracle:
            dataBuilder = std::make_shared<Data::Oracle::OracleDataBuilder>(user, password, database);
            break;
    }

    std::shared_ptr<IVehicleRepository> vehicleRepository = dataBuilder->GetVehicleRepository();
    std::shared_ptr<ICustomerRepository> customerRepository = dataBuilder->GetCustomerRepository();
    std::shared_ptr<IRentRepository> rentRepository = dataBuilder->GetRentRepository();

    Services services;
    services.Vehicle = std::make_shared<VehicleService>(vehicleRepository);
    services.Customer = std::make_shared<CustomerService>(customerRepository);
    services.Rent = std::make_shared<RentService>(rentRepository, services.Vehicle, services.Customer);

    std::shared_ptr<IViewBuilder> viewBuilder = std::make_shared<GtkViewBuilder>();

    Application application;

    auto applicationPresenter = std::make_shared<ApplicationPresenter>(&application, services, viewBuilder);
    applicationPresenter->Run();
    return 0;
}