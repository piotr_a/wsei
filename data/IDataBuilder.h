//
// Created by Adamczyk Piotr on 21.01.2020.
//

#ifndef WSEICARRENTAL_IDATABUILDER_H
#define WSEICARRENTAL_IDATABUILDER_H

#include <memory>
#include "../domain/IVehicleRepository.h"
#include "../domain/ICustomerRepository.h"
#include "../domain/IRentRepository.h"

class IDataBuilder {
public:
    virtual ~IDataBuilder() = default;

    virtual std::shared_ptr<IVehicleRepository> GetVehicleRepository() = 0;

    virtual std::shared_ptr<ICustomerRepository> GetCustomerRepository() = 0;

    virtual std::shared_ptr<IRentRepository> GetRentRepository() = 0;
};

#endif //WSEICARRENTAL_IDATABUILDER_H
