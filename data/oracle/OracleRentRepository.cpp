//
// Created by Adamczyk Piotr on 21.01.2020.
//

#include "OracleRentRepository.h"

Data::Oracle::OracleRentRepository::OracleRentRepository(std::shared_ptr<OracleConnection> &connection)
        : conn(connection->getConn()),
          env(connection->getEnv()) {

}

int Data::Oracle::OracleRentRepository::AddRent(Rent &rent) {

    std::string sql = "BEGIN INSERT INTO rents VALUES (rent_seq.NEXTVAL, :1, :2, :3, :4, :5, :6) RETURNING VID INTO :7; END;";

    oracle::occi::Statement *stmt = conn->createStatement(sql.c_str());
    int sqlRet = -1;
    try {
        stmt->setInt(1, rent.vehicleId);
        stmt->setInt(2, rent.customerId);
        stmt->setInt(3, rent.returned ? 1 : 0);
        stmt->setInt(4, rent.price);
        struct tm d;
        localtime_s(&d, &rent.rentFrom);
        oracle::occi::Date date(env, d.tm_year + 1900, d.tm_mon + 1, d.tm_mday,
                                d.tm_hour, d.tm_min, d.tm_sec);
        stmt->setDate(5, date);
        struct tm dd;
        localtime_s(&dd, &rent.rentFrom);
        oracle::occi::Date dated(env, dd.tm_year + 1900, dd.tm_mon + 1, dd.tm_mday,
                                 dd.tm_hour, dd.tm_min, dd.tm_sec);
        stmt->setDate(6, dated);


        stmt->registerOutParam(7, oracle::occi::OCCIINT);

        stmt->executeUpdate();
        sqlRet = stmt->getInt(7);
    } catch (oracle::occi::SQLException &ex) {

    }
    conn->commit();
    conn->terminateStatement(stmt);
    return sqlRet;
}

bool Data::Oracle::OracleRentRepository::DeleteRent(int id) {
    std::string sql = "BEGIN DELETE FROM rents WHERE vid = :1; :2 := sql%rowcount; END;";
    oracle::occi::Statement *stmt = conn->createStatement(sql.c_str());
    int sqlRet = -1;
    try {
        stmt->setInt(1, id);
        stmt->registerOutParam(2, oracle::occi::OCCIINT);

        stmt->executeUpdate();
        sqlRet = stmt->getInt(2);
    } catch (oracle::occi::SQLException &ex) {

    }
    conn->commit();
    conn->terminateStatement(stmt);
    return sqlRet != 0;
}

std::optional<Rent> Data::Oracle::OracleRentRepository::GetRent(int id) {
    std::vector<Rent> rents;
    std::string sql = "SELECT * FROM rents WHERE VID = " + std::to_string(id);
    getRentdb(rents, sql);

    if (rents.empty()) return std::nullopt;
    return rents[0];
}

std::vector<Rent> Data::Oracle::OracleRentRepository::GetRents() {
    std::vector<Rent> rents;
    std::string sql = "SELECT * FROM rents";
    getRentdb(rents, sql);

    return rents;
}

void Data::Oracle::OracleRentRepository::getRentdb(std::vector<Rent> &rents, const std::string &sql) {
    oracle::occi::Statement *stmt = this->conn->createStatement(sql.c_str());
    oracle::occi::ResultSet *rset = stmt->executeQuery();
    try {
        while (rset->next()) {
            Rent rent;
            rent.Id = rset->getInt(1);
            rent.vehicleId = rset->getInt(2);
            rent.customerId = rset->getInt(3);
            rent.returned = (rset->getInt(4) != 0);
            rent.price = rset->getInt(5);
            oracle::occi::Date date = rset->getDate(6);
            struct tm d;
            date.getDate(d.tm_year,
                         reinterpret_cast<unsigned int &>(d.tm_mon),
                         reinterpret_cast<unsigned int &>(d.tm_mday),
                         reinterpret_cast<unsigned int &>(d.tm_hour),
                         reinterpret_cast<unsigned int &>(d.tm_min),
                         reinterpret_cast<unsigned int &>(d.tm_sec));
            d.tm_year -= 1900;
            d.tm_mon--;
            rent.rentFrom = mktime(&d);
            oracle::occi::Date dated = rset->getDate(7);
            struct tm dd;
            dated.getDate(dd.tm_year,
                          reinterpret_cast<unsigned int &>(dd.tm_mon),
                          reinterpret_cast<unsigned int &>(dd.tm_mday),
                          reinterpret_cast<unsigned int &>(dd.tm_hour),
                          reinterpret_cast<unsigned int &>(dd.tm_min),
                          reinterpret_cast<unsigned int &>(dd.tm_sec));
            d.tm_year -= 1900;
            d.tm_mon--;
            rent.rentTo = mktime(&dd);

            rents.emplace_back(rent);
        }
    } catch (oracle::occi::SQLException &ex) {
    }
    conn->commit();
    stmt->closeResultSet(rset);
    this->conn->terminateStatement(stmt);
}
