//
// Created by Adamczyk Piotr on 21.01.2020.
//

#ifndef WSEICARRENTAL_ORACLEDATABUILDER_H
#define WSEICARRENTAL_ORACLEDATABUILDER_H

#include "../IDataBuilder.h"
#include "OracleConnection.h"


namespace Data::Oracle {
    class OracleDataBuilder : public IDataBuilder {
        std::shared_ptr<Data::Oracle::OracleConnection> connection;
    public:
        OracleDataBuilder(const std::string &user, const std::string &password, const std::string &database);

        std::shared_ptr<IVehicleRepository> GetVehicleRepository() override;

        std::shared_ptr<ICustomerRepository> GetCustomerRepository() override;

        std::shared_ptr<IRentRepository> GetRentRepository() override;

    };
}


#endif //WSEICARRENTAL_ORACLEDATABUILDER_H
