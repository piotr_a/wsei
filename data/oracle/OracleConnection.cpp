//
// Created by Adamczyk Piotr on 07.01.2020.
//

#include "OracleConnection.h"

Data::Oracle::OracleConnection::OracleConnection(const std::string &user, const std::string &password,
                                                 const std::string &database) {
    env = oracle::occi::Environment::createEnvironment();
    conn = env->createConnection(user, password, database);

}

Data::Oracle::OracleConnection::~OracleConnection() {

}

oracle::occi::Connection *Data::Oracle::OracleConnection::getConn() {
    return conn;
}

oracle::occi::Environment *Data::Oracle::OracleConnection::getEnv() const {
    return env;
}
