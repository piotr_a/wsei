//
// Created by Adamczyk Piotr on 07.01.2020.
//

#ifndef WSEICARRENTAL_ORACLECONNECTION_H
#define WSEICARRENTAL_ORACLECONNECTION_H

#include <occi.h>


namespace Data::Oracle {
    class OracleConnection {
    private:

        oracle::occi::Environment *env;
        oracle::occi::Connection *conn;
    public:
        oracle::occi::Environment *getEnv() const;

    public:
        OracleConnection(const std::string &user, const std::string &password, const std::string &database);

        ~OracleConnection();

        oracle::occi::Connection *getConn();
    };
}


#endif //WSEICARRENTAL_ORACLECONNECTION_H
