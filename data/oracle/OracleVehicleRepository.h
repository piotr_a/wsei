//
// Created by Adamczyk Piotr on 07.01.2020.
//

#ifndef WSEICARRENTAL_INMEMORYVEHICLEREPOSITORY_H
#define WSEICARRENTAL_VEHICLEREPOSITORY_H

#include <memory>
#include <vector>
#include "../../domain/IVehicleRepository.h"
#include "OracleConnection.h"
#include "../../domain/Vehicle.h"

namespace Data::Oracle {
    class OracleVehicleRepository : public IVehicleRepository {
    private:
        oracle::occi::Connection *conn;

        void getVehiclesdb(std::vector<Vehicle> &vehicles, const std::string &sql);

    public:
        OracleVehicleRepository(std::shared_ptr<OracleConnection> &connection);

        int AddVehicle(Vehicle &vehicle) override;

        bool DeleteVehicle(int id) override;

        std::optional<Vehicle> GetVehicle(int id) override;

        std::vector<Vehicle> GetVehicles() override;
    };
}


#endif //WSEICARRENTAL_INMEMORYVEHICLEREPOSITORY_H
namespace Data::InMemory {
    class InMemoryVehicleRepository : public IVehicleRepository {
    private:
        int idSequence;
        std::vector<Vehicle> vehicles;
    public:
        InMemoryVehicleRepository();

        int AddVehicle(Vehicle &vehicle) override;

        bool DeleteVehicle(int id) override;

        std::optional<Vehicle> GetVehicle(int id) override;

        std::vector<Vehicle> GetVehicles() override;
    };
}