//
// Created by Adamczyk Piotr on 07.01.2020.
//

#include "OracleVehicleRepository.h"

Data::Oracle::OracleVehicleRepository::OracleVehicleRepository(std::shared_ptr<OracleConnection> &connection)
        : conn(connection->getConn()) {

}

int Data::Oracle::OracleVehicleRepository::AddVehicle(Vehicle &vehicle) {

    std::string sql = "BEGIN INSERT INTO vehicles VALUES (vehicle_seq.NEXTVAL, :1, :2, :3, :4) RETURNING VID INTO :5; END;";

    oracle::occi::Statement *stmt = conn->createStatement(sql.c_str());
    int sqlRet = -1;
    try {
        stmt->setString(1, vehicle.plate);
        stmt->setString(2, vehicle.type);
        stmt->setString(3, vehicle.description);
        stmt->setInt(4, vehicle.price);
        stmt->registerOutParam(5, oracle::occi::OCCIINT);

        stmt->executeUpdate();
        sqlRet = stmt->getInt(5);
    } catch (oracle::occi::SQLException &ex) {

    }

    conn->terminateStatement(stmt);
    return sqlRet;
}

bool Data::Oracle::OracleVehicleRepository::DeleteVehicle(int id) {
    std::string sql = "BEGIN DELETE FROM vehicles WHERE vid = :1; :2 := sql%rowcount; END;";
    oracle::occi::Statement *stmt = conn->createStatement(sql.c_str());
    int sqlRet = -1;
    try {
        stmt->setInt(1, id);
        stmt->registerOutParam(2, oracle::occi::OCCIINT);

        stmt->executeUpdate();
        sqlRet = stmt->getInt(2);
    } catch (oracle::occi::SQLException &ex) {

    }
    conn->commit();
    conn->terminateStatement(stmt);
    return sqlRet != 0;
}

std::optional<Vehicle> Data::Oracle::OracleVehicleRepository::GetVehicle(int id) {
    std::vector<Vehicle> vehicles;
    std::string sql = "SELECT * FROM vehicles WHERE VID = " + std::to_string(id);
    getVehiclesdb(vehicles, sql);

    if (vehicles.empty()) return std::nullopt;
    return vehicles[0];
}

std::vector<Vehicle> Data::Oracle::OracleVehicleRepository::GetVehicles() {

    std::vector<Vehicle> vehicles;
    std::string sql = "SELECT * FROM vehicles";
    getVehiclesdb(vehicles, sql);

    return vehicles;
}

void Data::Oracle::OracleVehicleRepository::getVehiclesdb(std::vector<Vehicle> &vehicles, const std::string &sql) {
    oracle::occi::Statement *stmt = this->conn->createStatement(sql.c_str());
    oracle::occi::ResultSet *rset = stmt->executeQuery();
    try {
        while (rset->next()) {
            Vehicle vehicle;
            vehicle.id = rset->getInt(1);
            vehicle.plate = rset->getString(2);
            vehicle.type = rset->getString(3);
            vehicle.description = rset->getString(4);
            vehicle.price = rset->getInt(5);
            vehicles.emplace_back(vehicle);
        }
    } catch (oracle::occi::SQLException &ex) {
    }
    conn->commit();
    stmt->closeResultSet(rset);
    this->conn->terminateStatement(stmt);
}
