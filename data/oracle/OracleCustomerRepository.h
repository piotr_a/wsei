//
// Created by Adamczyk Piotr on 21.01.2020.
//

#ifndef WSEICARRENTAL_INMEMORYCUSTOMERREPOSITORY_H
#define WSEICARRENTAL_CUSTOMERREPOSITORY_H


#include <memory>
#include "../../domain/ICustomerRepository.h"
#include "OracleConnection.h"

namespace Data::Oracle {
    class OracleCustomerRepository : public ICustomerRepository {

    private:
        oracle::occi::Connection *conn;

        void getCustomerdb(std::vector<Customer> &customers, const std::string &sql);

    public:
        OracleCustomerRepository(std::shared_ptr<OracleConnection> &connection);

        int AddCustomer(Customer &customer) override;

        bool DeleteCustomer(int id) override;

        std::optional<Customer> GetCustomer(int id) override;

        std::vector<Customer> GetCustomers() override;
    };
}


#endif //WSEICARRENTAL_INMEMORYCUSTOMERREPOSITORY_H
