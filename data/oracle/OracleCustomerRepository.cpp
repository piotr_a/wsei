//
// Created by Adamczyk Piotr on 21.01.2020.
//

#include "OracleCustomerRepository.h"

Data::Oracle::OracleCustomerRepository::OracleCustomerRepository(std::shared_ptr<OracleConnection> &connection)
        : conn(connection->getConn()) {

}

int Data::Oracle::OracleCustomerRepository::AddCustomer(Customer &customer) {
    std::string sql = "BEGIN INSERT INTO customers VALUES (customer_seq.NEXTVAL, :1, :2, :3, :4) RETURNING VID INTO :5; END;";

    oracle::occi::Statement *stmt = conn->createStatement(sql.c_str());
    int sqlRet = -1;
    try {
        stmt->setString(1, customer.Name);
        stmt->setString(2, customer.Surname);
        stmt->setString(3, customer.Address);
        stmt->setString(4, customer.PhoneNumber);
        stmt->registerOutParam(5, oracle::occi::OCCIINT);

        stmt->executeUpdate();
        sqlRet = stmt->getInt(5);
    } catch (oracle::occi::SQLException &ex) {

    }
    conn->commit();
    conn->terminateStatement(stmt);
    return sqlRet;
}

bool Data::Oracle::OracleCustomerRepository::DeleteCustomer(int id) {
    std::string sql = "BEGIN DELETE FROM customers WHERE vid = :1; :2 := sql%rowcount; END;";
    oracle::occi::Statement *stmt = conn->createStatement(sql.c_str());
    int sqlRet = -1;
    try {
        stmt->setInt(1, id);
        stmt->registerOutParam(2, oracle::occi::OCCIINT);

        stmt->executeUpdate();
        sqlRet = stmt->getInt(2);
    } catch (oracle::occi::SQLException &ex) {

    }
    conn->commit();
    conn->terminateStatement(stmt);
    return sqlRet != 0;
}

std::optional<Customer> Data::Oracle::OracleCustomerRepository::GetCustomer(int id) {
    std::vector<Customer> customers;
    std::string sql = "SELECT * FROM customers WHERE VID = " + std::to_string(id);
    getCustomerdb(customers, sql);

    if (customers.empty()) return std::nullopt;
    return customers[0];
}

std::vector<Customer> Data::Oracle::OracleCustomerRepository::GetCustomers() {
    std::vector<Customer> customers;
    std::string sql = "SELECT * FROM customers";
    getCustomerdb(customers, sql);

    return customers;
}

void Data::Oracle::OracleCustomerRepository::getCustomerdb(std::vector<Customer> &customers, const std::string &sql) {
    oracle::occi::Statement *stmt = this->conn->createStatement(sql.c_str());
    oracle::occi::ResultSet *rset = stmt->executeQuery();
    try {
        while (rset->next()) {
            Customer customer;
            customer.Id = rset->getInt(1);
            customer.Name = rset->getString(2);
            customer.Surname = rset->getString(3);
            customer.Address = rset->getString(4);
            customer.PhoneNumber = rset->getString(5);
            customers.emplace_back(customer);
        }
    } catch (oracle::occi::SQLException &ex) {
    }
    conn->commit();
    stmt->closeResultSet(rset);
    this->conn->terminateStatement(stmt);
}
