//
// Created by Adamczyk Piotr on 21.01.2020.
//

#include "OracleDataBuilder.h"
#include "OracleCustomerRepository.h"
#include "OracleRentRepository.h"
#include "OracleVehicleRepository.h"

Data::Oracle::OracleDataBuilder::OracleDataBuilder(const std::string &user, const std::string &password,
                                                   const std::string &database) {
    connection = std::make_shared<Data::Oracle::OracleConnection>(user, password, database);
}

std::shared_ptr<IVehicleRepository> Data::Oracle::OracleDataBuilder::GetVehicleRepository() {
    return std::make_shared<OracleVehicleRepository>(connection);
}

std::shared_ptr<ICustomerRepository> Data::Oracle::OracleDataBuilder::GetCustomerRepository() {
    return std::make_shared<OracleCustomerRepository>(connection);
}

std::shared_ptr<IRentRepository> Data::Oracle::OracleDataBuilder::GetRentRepository() {
    return std::make_shared<OracleRentRepository>(connection);
}
