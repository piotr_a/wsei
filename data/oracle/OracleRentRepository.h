//
// Created by Adamczyk Piotr on 21.01.2020.
//

#ifndef WSEICARRENTAL_INMEMORYRENTREPOSITORY_H
#define WSEICARRENTAL_RENTREPOSITORY_H


#include <memory>
#include "../../domain/IRentRepository.h"
#include "OracleConnection.h"

namespace Data::Oracle {
    class OracleRentRepository : public IRentRepository {
    private:
        oracle::occi::Connection *conn;
        oracle::occi::Environment *env;

        void getRentdb(std::vector<Rent> &rents, const std::string &sql);

    public:
        OracleRentRepository(std::shared_ptr<OracleConnection> &connection);

        int AddRent(Rent &rent) override;

        bool DeleteRent(int id) override;

        std::optional<Rent> GetRent(int id) override;

        std::vector<Rent> GetRents() override;

    };
}


#endif //WSEICARRENTAL_INMEMORYRENTREPOSITORY_H
