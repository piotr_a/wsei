//
// Created by Adamczyk Piotr on 29.11.2019.
//

#include <algorithm>
#include "InMemoryVehicleRepository.h"

int Data::InMemory::InMemoryVehicleRepository::AddVehicle(Vehicle &vehicle) {
    vehicle.id = ++idSequence;
    vehicles.push_back(vehicle);
    return vehicle.id;
}

bool Data::InMemory::InMemoryVehicleRepository::DeleteVehicle(int id) {
    auto it = std::find_if(vehicles.begin(), vehicles.end(), [id](const Vehicle &e) { return id == e.id; });

    if (it != vehicles.end()) {
        vehicles.erase(it);
        return true;
    }
    return false;
}

std::vector<Vehicle> Data::InMemory::InMemoryVehicleRepository::GetVehicles() {
    return vehicles;
}

std::optional<Vehicle> Data::InMemory::InMemoryVehicleRepository::GetVehicle(int id) {
    auto it = std::find_if(vehicles.begin(), vehicles.end(), [id](const Vehicle &e) { return id == e.id; });

    if (it != vehicles.end()) {
        return *it;
    }
    return std::nullopt;
}

Data::InMemory::InMemoryVehicleRepository::InMemoryVehicleRepository() : idSequence(0) {
    vehicles.push_back({++idSequence, "LU 11111", "brak1", "d1", 10000});
    vehicles.push_back({++idSequence, "LU 22222", "brak2", "d2", 1090});
    vehicles.push_back({++idSequence, "LU 33333", "brak3", "d3", 5020});
    vehicles.push_back({++idSequence, "LU 44444", "brak4", "d4", 6000});
    vehicles.push_back({++idSequence, "LU 55555", "brak5", "d5", 9999});
}
