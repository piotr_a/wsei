//
// Created by Adamczyk Piotr on 06.12.2019.
//

#ifndef WSEICARRENTAL_INMEMORYCUSTOMERREPOSITORY_H
#define WSEICARRENTAL_INMEMORYCUSTOMERREPOSITORY_H


#include "../../domain/ICustomerRepository.h"

namespace Data::InMemory {
    class InMemoryCustomerRepository : public ICustomerRepository {
    private:
        int idSequence;
        std::vector<Customer> customers;
    public:
        InMemoryCustomerRepository();

        int AddCustomer(Customer &customer) override;

        bool DeleteCustomer(int id) override;

        std::optional<Customer> GetCustomer(int id) override;

        std::vector<Customer> GetCustomers() override;
    };
}


#endif //WSEICARRENTAL_INMEMORYCUSTOMERREPOSITORY_H
