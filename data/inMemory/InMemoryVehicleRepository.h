//
// Created by Adamczyk Piotr on 29.11.2019.
//

#ifndef WSEICARRENTAL_INMEMORYVEHICLEREPOSITORY_H
#define WSEICARRENTAL_INMEMORYVEHICLEREPOSITORY_H


#include <vector>
#include "../../domain/Vehicle.h"
#include "../../domain/IVehicleRepository.h"

namespace Data::InMemory {
    class InMemoryVehicleRepository : public IVehicleRepository {
    private:
        int idSequence;
        std::vector<Vehicle> vehicles;
    public:
        InMemoryVehicleRepository();

        int AddVehicle(Vehicle &vehicle) override;

        bool DeleteVehicle(int id) override;

        std::optional<Vehicle> GetVehicle(int id) override;

        std::vector<Vehicle> GetVehicles() override;
    };
}

#endif //WSEICARRENTAL_INMEMORYVEHICLEREPOSITORY_H
