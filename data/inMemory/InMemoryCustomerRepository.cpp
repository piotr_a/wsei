//
// Created by Adamczyk Piotr on 06.12.2019.
//

#include <algorithm>
#include "InMemoryCustomerRepository.h"

int Data::InMemory::InMemoryCustomerRepository::AddCustomer(Customer &customer) {
    customer.Id = ++idSequence;
    customers.push_back(customer);
    return customer.Id;
}

bool Data::InMemory::InMemoryCustomerRepository::DeleteCustomer(int id) {
    auto it = std::find_if(customers.begin(), customers.end(), [id](const Customer &e) { return id == e.Id; });

    if (it != customers.end()) {
        customers.erase(it);
        return true;
    }
    return false;
}

std::vector<Customer> Data::InMemory::InMemoryCustomerRepository::GetCustomers() {
    return customers;
}

std::optional<Customer> Data::InMemory::InMemoryCustomerRepository::GetCustomer(int id) {
    auto it = std::find_if(customers.begin(), customers.end(), [id](const Customer &e) { return id == e.Id; });

    if (it != customers.end()) {
        return *it;
    }
    return std::nullopt;
}

Data::InMemory::InMemoryCustomerRepository::InMemoryCustomerRepository() : idSequence(0) {
    customers.push_back({++idSequence, "Alan", "Albertowicz", "Mieszka", "111-222-333"});
    customers.push_back({++idSequence, "Brajan", "Brexit", "UK", "222"});
    customers.push_back({++idSequence, "Celina", "Ciupak", "Mosty", "333"});
    customers.push_back({++idSequence, "Dominika", "Dymkowska", "UK", "456-456-444"});
    customers.push_back({++idSequence, "Endrju", "Ejsmont", "UK", "565-454-343"});
}
