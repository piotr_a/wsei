//
// Created by Adamczyk Piotr on 21.01.2020.
//

#ifndef WSEICARRENTAL_INMEMORYDATABUILDER_H
#define WSEICARRENTAL_INMEMORYDATABUILDER_H


#include "../IDataBuilder.h"

namespace Data::InMemory {
    class InMemoryDataBuilder : public IDataBuilder {
    public:

        std::shared_ptr<IVehicleRepository> GetVehicleRepository() override;

        std::shared_ptr<ICustomerRepository> GetCustomerRepository() override;

        std::shared_ptr<IRentRepository> GetRentRepository() override;

    };
}


#endif //WSEICARRENTAL_INMEMORYDATABUILDER_H
