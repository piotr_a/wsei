//
// Created by Adamczyk Piotr on 21.01.2020.
//

#include "InMemoryDataBuilder.h"
#include "InMemoryCustomerRepository.h"
#include "InMemoryRentRepository.h"
#include "InMemoryVehicleRepository.h"

std::shared_ptr<IVehicleRepository> Data::InMemory::InMemoryDataBuilder::GetVehicleRepository() {
    return std::make_shared<Data::InMemory::InMemoryVehicleRepository>();
}

std::shared_ptr<ICustomerRepository> Data::InMemory::InMemoryDataBuilder::GetCustomerRepository() {
    return std::make_shared<Data::InMemory::InMemoryCustomerRepository>();
}

std::shared_ptr<IRentRepository> Data::InMemory::InMemoryDataBuilder::GetRentRepository() {
    return std::make_shared<Data::InMemory::InMemoryRentRepository>();
}
