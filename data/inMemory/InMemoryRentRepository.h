//
// Created by Adamczyk Piotr on 18.12.2019.
//

#ifndef WSEICARRENTAL_INMEMORYRENTREPOSITORY_H
#define WSEICARRENTAL_INMEMORYRENTREPOSITORY_H


#include "../../domain/IRentRepository.h"

namespace Data::InMemory {
    class InMemoryRentRepository : public IRentRepository {
    private:
        int idSequence;
        std::vector<Rent> rents;
    public:
        InMemoryRentRepository();

        int AddRent(Rent &rent) override;

        bool DeleteRent(int id) override;

        std::optional<Rent> GetRent(int id) override;

        std::vector<Rent> GetRents() override;

    };
}


#endif //WSEICARRENTAL_INMEMORYRENTREPOSITORY_H
