//
// Created by Adamczyk Piotr on 18.12.2019.
//

#include <algorithm>
#include "InMemoryRentRepository.h"

int Data::InMemory::InMemoryRentRepository::AddRent(Rent &rent) {
    rent.Id = ++idSequence;
    rents.push_back(rent);
    return rent.Id;
}

bool Data::InMemory::InMemoryRentRepository::DeleteRent(int id) {
    auto it = std::find_if(rents.begin(), rents.end(), [id](const Rent &e) { return id == e.Id; });

    if (it != rents.end()) {
        rents.erase(it);
        return true;
    }
    return false;
}

std::optional<Rent> Data::InMemory::InMemoryRentRepository::GetRent(int id) {
    auto it = std::find_if(rents.begin(), rents.end(), [id](const Rent &e) { return id == e.Id; });

    if (it != rents.end()) {
        return *it;
    }
    return std::nullopt;
}

std::vector<Rent> Data::InMemory::InMemoryRentRepository::GetRents() {
    return rents;
}

Data::InMemory::InMemoryRentRepository::InMemoryRentRepository() : idSequence(0) {
    rents.push_back({++idSequence, 1, 1, true, 10000, 1577886892, 1578146092});
    rents.push_back({++idSequence, 2, 2, false, 1090, 1577886892, 0});
    rents.push_back({++idSequence, 3, 3, false, 5020, 1577886892, 0});
    rents.push_back({++idSequence, 4, 4, true, 6000, 1577886892, 1578146092});
    rents.push_back({++idSequence, 5, 5, true, 9999, 1577886892, 1578146092});
}
