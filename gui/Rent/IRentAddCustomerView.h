//
// Created by Adamczyk Piotr on 09.12.2019.
//

#ifndef WSEICARRENTAL_IRENTADDCUSTOMERVIEW_H
#define WSEICARRENTAL_IRENTADDCUSTOMERVIEW_H


#include <string>

class IRentAddCustomerView {
public:
    virtual ~IRentAddCustomerView() = default;

    virtual std::string getName() = 0;

    virtual std::string getSurname() = 0;

    virtual std::string getAddress() = 0;

    virtual std::string getPhoneNumber() = 0;

    virtual void clear() = 0;
};


#endif //WSEICARRENTAL_IRENTADDCUSTOMERVIEW_H
