//
// Created by Adamczyk Piotr on 04.01.2020.
//

#ifndef WSEICARRENTAL_IRENTCUSTOMERLISTPRESENTER_H
#define WSEICARRENTAL_IRENTCUSTOMERLISTPRESENTER_H

class IRentCustomerListPresenter {
public:
    virtual ~IRentCustomerListPresenter() = default;

    virtual void SelectCustomer(int id) = 0;

    virtual void DeleteCustomer(int id) = 0;

    virtual void AddCustomer() = 0;
};

#endif //WSEICARRENTAL_IRENTCUSTOMERLISTPRESENTER_H
