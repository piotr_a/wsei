//
// Created by Adamczyk Piotr on 04.01.2020.
//

#include "RentPresenter.h"

RentPresenter::RentPresenter(Application *application, Services &services, std::shared_ptr<IViewBuilder> &viewBuilder)
        : application(application) {
    view = viewBuilder->GetRentView(this);

    rentVehicleListPresenter = std::make_shared<RentVehicleListPresenter>(application, services, viewBuilder);
    rentCustomerListPresenter = std::make_shared<RentCustomerListPresenter>(application, services, viewBuilder);
    rentAddCustomerPresenter = std::make_shared<RentAddCustomerPresenter>(application, services, viewBuilder);
    rentSummaryPresenter = std::make_shared<RentSummaryPresenter>(application, services, viewBuilder);

    application->rentVehicleState.onVehicleChange = [&](
            bool selected) { if (selected) { CustomerListView(); } else { VehicleListView(); }};
    application->rentVehicleState.onCustomerChange = [&](
            bool selected) { if (selected) { RentSummaryView(); } else { CustomerListView(); }};

    application->rentVehicleState.onCustomerAdd = [&] { CustomerAddView(); };
    application->rentVehicleState.onClear = [&] { VehicleListView(); };
}


void RentPresenter::VehicleListView() const {
    if (view) {
        view->ShowVehicleList();
    }
}

void RentPresenter::CustomerListView() const {
    if (view) {
        view->ShowCustomerList();
    }
}

void RentPresenter::CustomerAddView() const {
    if (view) {
        view->ShowCustomerAdd();

    }
}

void RentPresenter::RentSummaryView() const {
    if (view) {
        view->ShowRentSummary();
        rentSummaryPresenter->UpdateView();
    }
}

void RentPresenter::SelectedVehicle(int id) {
    application->handle(Event::Rent::SelectVehicle{id});
}

void RentPresenter::SelectedCustomer(int id) {
    application->handle(Event::Rent::SelectCustomer{id});
}