//
// Created by Adamczyk Piotr on 06.12.2019.
//

#ifndef WSEICARRENTAL_RENTCUSTOMERLISTPRESENTER_H
#define WSEICARRENTAL_RENTCUSTOMERLISTPRESENTER_H


#include <memory>
#include "IRentCustomerListView.h"
#include "../../domain/Application.h"
#include "../IViewBuilder.h"
#include "../../domain/Services.h"

class RentCustomerListPresenter : public IRentCustomerListPresenter {
    std::shared_ptr<CustomerService> customerService;
    std::shared_ptr<IRentCustomerListView> view;
    Application *application;

    CustomerService::Observer observer;
public:
    explicit RentCustomerListPresenter(Application *application, Services &services,
                                       std::shared_ptr<IViewBuilder> &viewBuilder);

    ~RentCustomerListPresenter() override;

    void SelectCustomer(int id) override;

    void DeleteCustomer(int id) override;

    void AddCustomer() override;

    void UpdateView() const;
};


#endif //WSEICARRENTAL_RENTCUSTOMERLISTPRESENTER_H
