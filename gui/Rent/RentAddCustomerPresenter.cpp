//
// Created by wsei on 07.12.2019.
//

#include "RentAddCustomerPresenter.h"


RentAddCustomerPresenter::RentAddCustomerPresenter(Application *application,
                                                   Services &services,
                                                   std::shared_ptr<IViewBuilder> &viewBuilder)
        : application(application),
          customerService(services.Customer) {
    view = viewBuilder->GetRentAddCustomerView(this);
    clear();
}


void RentAddCustomerPresenter::AddCustomer() {
    if (view) {
        int id = customerService->AddCustomer(view->getName(),
                                              view->getSurname(),
                                              view->getAddress(),
                                              view->getPhoneNumber());
        application->handle(Event::Rent::SelectCustomer{id});
    }
}

void RentAddCustomerPresenter::Cancel() {
    application->handle(Event::Rent::SelectCustomer{0});
}

void RentAddCustomerPresenter::clear() {
    if (view) {
        view->clear();
    }
}