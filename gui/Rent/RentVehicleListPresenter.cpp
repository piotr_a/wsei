//
// Created by Adamczyk Piotr on 03.12.2019.
//
#include "RentVehicleListPresenter.h"
#include "../../domain/Events/RentEvents.h"
#include <algorithm>

RentVehicleListPresenter::RentVehicleListPresenter(Application *application,
                                                   Services &services,
                                                   std::shared_ptr<IViewBuilder> &viewBuilder)
        : application(application),
          vehicleService(services.Vehicle),
          rentService(services.Rent) {
    view = viewBuilder->GetRentVehicleListView(this);
    UpdateView();
    vehicleObserver = vehicleService->Register([&] { UpdateView(); });
    rentObserver = rentService->Register([&] { UpdateView(); });
}


RentVehicleListPresenter::~RentVehicleListPresenter() {
    vehicleService->Remove(vehicleObserver);
    rentService->Remove(rentObserver);
}

void RentVehicleListPresenter::SelectVehicle(int id) {
    application->handle(Event::Rent::SelectVehicle{id});
}

void RentVehicleListPresenter::UpdateView() const {
    if (view) {
        auto vehicles = vehicleService->GetVehicles();
        vehicles.erase(std::remove_if(vehicles.begin(), vehicles.end(),
                                      [&](VehicleDto &v) {
                                          return rentService->IsVehicleRented(v.id);
                                      }), vehicles.end());
        view->UpdateVehicles(vehicles);
    }
}




