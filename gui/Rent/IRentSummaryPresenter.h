//
// Created by Adamczyk Piotr on 04.01.2020.
//

#ifndef WSEICARRENTAL_IRENTSUMMARYPRESENTER_H
#define WSEICARRENTAL_IRENTSUMMARYPRESENTER_H

class IRentSummaryPresenter {
public:
    virtual ~IRentSummaryPresenter() = default;

    virtual void Rent() = 0;

    virtual void Cancel() = 0;
};

#endif //WSEICARRENTAL_IRENTSUMMARYPRESENTER_H
