//
// Created by Adamczyk Piotr on 06.12.2019.
//

#include "RentCustomerListPresenter.h"


RentCustomerListPresenter::RentCustomerListPresenter(Application *application,
                                                     Services &services,
                                                     std::shared_ptr<IViewBuilder> &viewBuilder)
        : application(application),
          customerService(services.Customer) {
    view = viewBuilder->GetRentCustomerListView(this);
    UpdateView();
    observer = customerService->Register([&] { UpdateView(); });
}

RentCustomerListPresenter::~RentCustomerListPresenter() {
    customerService->Remove(observer);
}


void RentCustomerListPresenter::SelectCustomer(int id) {
    application->handle(Event::Rent::SelectCustomer{id});
}

void RentCustomerListPresenter::DeleteCustomer(int id) {
    customerService->DeleteCustomer(id);
}

void RentCustomerListPresenter::AddCustomer() {
    application->handle(Event::Rent::AddCustomer{});
}

void RentCustomerListPresenter::UpdateView() const {
    if (view) {
        view->UpdateCustomers(customerService->GetCustomers());
    }
}
