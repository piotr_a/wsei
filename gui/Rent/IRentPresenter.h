//
// Created by Adamczyk Piotr on 04.01.2020.
//

#ifndef WSEICARRENTAL_IRENTPRESENTER_H
#define WSEICARRENTAL_IRENTPRESENTER_H

class IRentPresenter {
public:
    virtual ~IRentPresenter() = default;
};

#endif //WSEICARRENTAL_IRENTPRESENTER_H
