//
// Created by wsei on 07.12.2019.
//

#ifndef WSEICARRENTAL_RENTADDCUSTOMERPRESENTER_H
#define WSEICARRENTAL_RENTADDCUSTOMERPRESENTER_H

#include <memory>
#include "../../domain/Application.h"
#include "IRentAddCustomerView.h"
#include "../IViewBuilder.h"
#include "../../domain/Services.h"

class RentAddCustomerPresenter : public IRentAddCustomerPresenter {
private:
    std::shared_ptr<CustomerService> customerService;
    std::shared_ptr<IRentAddCustomerView> view;
    Application *application;
public:
    explicit RentAddCustomerPresenter(Application *application, Services &services,
                                      std::shared_ptr<IViewBuilder> &viewBuilder);

    void AddCustomer() override;

    void Cancel() override;

    void clear();

};


#endif //WSEICARRENTAL_RENTADDCUSTOMERPRESENTER_H
