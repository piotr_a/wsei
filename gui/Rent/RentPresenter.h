//
// Created by Adamczyk Piotr on 04.01.2020.
//

#ifndef WSEICARRENTAL_RENTPRESENTER_H
#define WSEICARRENTAL_RENTPRESENTER_H


#include "../../domain/Application.h"
#include "../../domain/Services.h"
#include "IRentView.h"
#include "../IViewBuilder.h"
#include "RentVehicleListPresenter.h"
#include "RentCustomerListPresenter.h"
#include "RentAddCustomerPresenter.h"
#include "RentSummaryPresenter.h"

class RentPresenter : public IRentPresenter {
private:
    std::shared_ptr<IRentView> view;
    Application *application;

    std::shared_ptr<RentVehicleListPresenter> rentVehicleListPresenter;
    std::shared_ptr<RentCustomerListPresenter> rentCustomerListPresenter;
    std::shared_ptr<RentAddCustomerPresenter> rentAddCustomerPresenter;
    std::shared_ptr<RentSummaryPresenter> rentSummaryPresenter;

public:
    explicit RentPresenter(Application *application, Services &services, std::shared_ptr<IViewBuilder> &viewBuilder);

    void VehicleListView() const;

    void CustomerListView() const;

    void CustomerAddView() const;

    void RentSummaryView() const;

    void SelectedVehicle(int id);

    void SelectedCustomer(int id);

//    void CustomerAddView();
//
//    void RentSummary();

};


#endif //WSEICARRENTAL_RENTPRESENTER_H
