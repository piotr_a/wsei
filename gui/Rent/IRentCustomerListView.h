//
// Created by Adamczyk Piotr on 06.12.2019.
//

#ifndef WSEICARRENTAL_IRENTCUSTOMERLISTVIEW_H
#define WSEICARRENTAL_IRENTCUSTOMERLISTVIEW_H


#include <vector>
#include "../../domain/CustomerDto.h"

class IRentCustomerListView {
public:
    virtual ~IRentCustomerListView() = default;

    virtual void UpdateCustomers(const std::vector<CustomerDto> &customersDtos) = 0;
};

#endif //WSEICARRENTAL_IRENTCUSTOMERLISTVIEW_H
