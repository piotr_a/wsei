//
// Created by Adamczyk Piotr on 04.01.2020.
//

#ifndef WSEICARRENTAL_IRENTVEHICLELISTPRESENTER_H
#define WSEICARRENTAL_IRENTVEHICLELISTPRESENTER_H

class IRentVehicleListPresenter {
public:
    virtual ~IRentVehicleListPresenter() = default;

    virtual void SelectVehicle(int id) = 0;
};

#endif //WSEICARRENTAL_IRENTVEHICLELISTPRESENTER_H
