//
// Created by Adamczyk Piotr on 10.12.2019.
//

#include "RentSummaryPresenter.h"

RentSummaryPresenter::RentSummaryPresenter(Application *application,
                                           Services &services,
                                           std::shared_ptr<IViewBuilder> &viewBuilder)
        : application(application),
          vehicleService(services.Vehicle),
          customerService(services.Customer),
          rentService(services.Rent) {
    view = viewBuilder->GetRentSummaryView(this);
}


void RentSummaryPresenter::Rent() {
    rentService->AddRent(application->rentVehicleState.vehicleId, application->rentVehicleState.customerId);
    application->handle(Event::Rent::Cancel{});
}

void RentSummaryPresenter::Cancel() {
    application->handle(Event::Rent::Cancel{});
}

void RentSummaryPresenter::UpdateView() const {
    if (view) {
        view->UpdateView(vehicleService->GetVehicle(application->rentVehicleState.vehicleId),
                         customerService->GetCustomer(application->rentVehicleState.customerId));
    }
}
