//
// Created by Adamczyk Piotr on 03.12.2019.
//

#ifndef WSEICARRENTAL_RENTVEHICLELISTPRESENTER_H
#define WSEICARRENTAL_RENTVEHICLELISTPRESENTER_H


#include "IRentVehicleListView.h"
#include "../../domain/Application.h"
#include "../IViewBuilder.h"
#include "../../domain/Services.h"

class RentVehicleListPresenter : public IRentVehicleListPresenter {
    Application *application;
    std::shared_ptr<VehicleService> vehicleService;
    std::shared_ptr<RentService> rentService;
    std::shared_ptr<IRentVehicleListView> view;

    VehicleService::Observer vehicleObserver;
    RentService::Observer rentObserver;
public:
    explicit RentVehicleListPresenter(Application *application, Services &services,
                                      std::shared_ptr<IViewBuilder> &viewBuilder);

    ~RentVehicleListPresenter() override;

    void SelectVehicle(int id) override;

    void UpdateView() const;
};


#endif //WSEICARRENTAL_RENTVEHICLELISTPRESENTER_H
