//
// Created by Adamczyk Piotr on 04.01.2020.
//

#ifndef WSEICARRENTAL_IRENTADDCUSTOMERPRESENTER_H
#define WSEICARRENTAL_IRENTADDCUSTOMERPRESENTER_H

class IRentAddCustomerPresenter {
public:
    virtual ~IRentAddCustomerPresenter() = default;

    virtual void AddCustomer() = 0;

    virtual void Cancel() = 0;
};

#endif //WSEICARRENTAL_IRENTADDCUSTOMERPRESENTER_H
