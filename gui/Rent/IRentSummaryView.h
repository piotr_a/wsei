//
// Created by Adamczyk Piotr on 10.12.2019.
//

#ifndef WSEICARRENTAL_IRENTSUMMARYVIEW_H
#define WSEICARRENTAL_IRENTSUMMARYVIEW_H

#include <string>
#include "../../domain/VehicleDto.h"
#include "../../domain/CustomerDto.h"

class IRentSummaryView {
public:
    virtual ~IRentSummaryView() = default;

    virtual void UpdateView(const VehicleDto &vehicleDto, const CustomerDto &customerDto) = 0;

    virtual void clear() = 0;
};

#endif //WSEICARRENTAL_IRENTSUMMARYVIEW_H
