//
// Created by Adamczyk Piotr on 04.01.2020.
//

#ifndef WSEICARRENTAL_IRENTVIEW_H
#define WSEICARRENTAL_IRENTVIEW_H

class IRentView {
public:
    virtual ~IRentView() = default;

    virtual void ShowVehicleList() = 0;

    virtual void ShowCustomerList() = 0;

    virtual void ShowCustomerAdd() = 0;

    virtual void ShowRentSummary() = 0;
};

#endif //WSEICARRENTAL_IRENTVIEW_H
