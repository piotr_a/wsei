//
// Created by Adamczyk Piotr on 10.12.2019.
//

#ifndef WSEICARRENTAL_RENTSUMMARYPRESENTER_H
#define WSEICARRENTAL_RENTSUMMARYPRESENTER_H


#include <memory>
#include "../../domain/VehicleService.h"
#include "../../domain/CustomerService.h"
#include "IRentSummaryView.h"
#include "../../domain/Application.h"
#include "../../domain/Services.h"
#include "../IViewBuilder.h"

class RentSummaryPresenter : public IRentSummaryPresenter {
    std::shared_ptr<VehicleService> vehicleService;
    std::shared_ptr<CustomerService> customerService;
    std::shared_ptr<RentService> rentService;
    std::shared_ptr<IRentSummaryView> view;
    Application *application;
public:
    RentSummaryPresenter(Application *application, Services &services, std::shared_ptr<IViewBuilder> &viewBuilder);

    void Rent() override;

    void Cancel() override;

    void UpdateView() const;
};


#endif //WSEICARRENTAL_RENTSUMMARYPRESENTER_H
