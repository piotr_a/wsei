//
// Created by Adamczyk Piotr on 03.12.2019.
//

#ifndef WSEICARRENTAL_IRENTVEHICLELISTVIEW_H
#define WSEICARRENTAL_IRENTVEHICLELISTVIEW_H


#include <vector>
#include "../../domain/VehicleDto.h"

class IRentVehicleListView {
public:
    virtual ~IRentVehicleListView() = default;

    virtual void UpdateVehicles(const std::vector<VehicleDto> &vehicleDtos) = 0;
};


#endif //WSEICARRENTAL_IRENTVEHICLELISTVIEW_H
