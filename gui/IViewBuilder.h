//
// Created by Adamczyk Piotr on 04.01.2020.
//

#ifndef WSEICARRENTAL_IVIEWBUILDER_H
#define WSEICARRENTAL_IVIEWBUILDER_H

#include <memory>
#include "IApplicationView.h"
#include "Dashboard/IDashboardView.h"
#include "Rent/IRentView.h"
#include "ReturnVehicle/IReturnVehicleView.h"
#include "Vehicles/IVehiclesView.h"
#include "Rent/IRentVehicleListView.h"
#include "Rent/IRentCustomerListView.h"
#include "Rent/IRentAddCustomerView.h"
#include "Rent/IRentSummaryView.h"
#include "Vehicles/IVehiclesAddView.h"

#include "IApplicationPresenter.h"
#include "Dashboard/IDashboardPresenter.h"
#include "Rent/IRentPresenter.h"
#include "ReturnVehicle/IReturnVehiclePresenter.h"
#include "Vehicles/IVehiclesPresenter.h"
#include "Rent/IRentVehicleListPresenter.h"
#include "Rent/IRentCustomerListPresenter.h"
#include "Rent/IRentAddCustomerPresenter.h"
#include "Rent/IRentSummaryPresenter.h"
#include "Vehicles/IVehiclesAddPresenter.h"

class IViewBuilder {
public:
    virtual ~IViewBuilder() = default;

    virtual std::shared_ptr<IApplicationView> GetApplicationView(IApplicationPresenter *presenter) = 0;

    virtual std::shared_ptr<IDashboardView> GetDashboardView(IDashboardPresenter *presenter) = 0;

    virtual std::shared_ptr<IRentView> GetRentView(IRentPresenter *presenter) = 0;

    virtual std::shared_ptr<IReturnVehicleView> GetReturnVehicleView(IReturnVehiclePresenter *presenter) = 0;

    virtual std::shared_ptr<IVehiclesView> GetVehiclesView(IVehiclesPresenter *presenter) = 0;

    virtual std::shared_ptr<IRentVehicleListView> GetRentVehicleListView(IRentVehicleListPresenter *presenter) = 0;

    virtual std::shared_ptr<IRentCustomerListView> GetRentCustomerListView(IRentCustomerListPresenter *presenter) = 0;

    virtual std::shared_ptr<IRentAddCustomerView> GetRentAddCustomerView(IRentAddCustomerPresenter *presenter) = 0;

    virtual std::shared_ptr<IRentSummaryView> GetRentSummaryView(IRentSummaryPresenter *presenter) = 0;

    virtual std::shared_ptr<IVehiclesAddView> GetVehiclesAddView(IVehiclesAddPresenter *presenter) = 0;
};

#endif //WSEICARRENTAL_IVIEWBUILDER_H
