//
// Created by Adamczyk Piotr on 04.01.2020.
//

#ifndef WSEICARRENTAL_IDASHBOARDPRESENTER_H
#define WSEICARRENTAL_IDASHBOARDPRESENTER_H

class IDashboardPresenter {
public:
    virtual ~IDashboardPresenter() = default;
};

#endif //WSEICARRENTAL_IDASHBOARDPRESENTER_H
