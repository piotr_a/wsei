//
// Created by Adamczyk Piotr on 02.01.2020.
//

#ifndef WSEICARRENTAL_DASHBOARDPRESENTER_H
#define WSEICARRENTAL_DASHBOARDPRESENTER_H

#include <memory>
#include "IDashboardView.h"
#include "../../domain/Services.h"
#include "../IViewBuilder.h"

class DashboardPresenter : public IDashboardPresenter {
private:
    std::shared_ptr<VehicleService> vehiclesService;
    std::shared_ptr<RentService> rentService;
    std::shared_ptr<IDashboardView> view;

    RentService::Observer rentObserver;
    VehicleService::Observer vehicleObserver;
public:
    explicit DashboardPresenter(Services &services, std::shared_ptr<IViewBuilder> &viewBuilder);

    ~DashboardPresenter() override;

    void UpdateView() const;
};

#endif //WSEICARRENTAL_DASHBOARDPRESENTER_H
