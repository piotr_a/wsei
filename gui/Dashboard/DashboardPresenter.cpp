//
// Created by Adamczyk Piotr on 02.01.2020.
//

#include "DashboardPresenter.h"


DashboardPresenter::DashboardPresenter(Services &services, std::shared_ptr<IViewBuilder> &viewBuilder)
        : vehiclesService(services.Vehicle),
          rentService(services.Rent) {

    view = viewBuilder->GetDashboardView(this);
    UpdateView();
    rentObserver = rentService->Register([&] { UpdateView(); });
    vehicleObserver = vehiclesService->Register([&] { UpdateView(); });
}

DashboardPresenter::~DashboardPresenter() {
    rentService->Remove(rentObserver);
    vehiclesService->Remove(vehicleObserver);
}

void DashboardPresenter::UpdateView() const {
    if (view) {
        auto rented = rentService->GetActiveRents().size();
        auto toRent = vehiclesService->GetVehicles().size() - rented;
        view->Update(rented, toRent);
    }
}
