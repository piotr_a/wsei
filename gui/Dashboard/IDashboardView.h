//
// Created by Adamczyk Piotr on 02.01.2020.
//

#ifndef WSEICARRENTAL_IDASHBOARDVIEW_H
#define WSEICARRENTAL_IDASHBOARDVIEW_H

class IDashboardView {
public:
    virtual ~IDashboardView() = default;

    virtual void Update(int count, int toRentCount) = 0;
};

#endif //WSEICARRENTAL_IDASHBOARDVIEW_H
