//
// Created by Adamczyk Piotr on 05.12.2019.
//

#include "ApplicationPresenter.h"


ApplicationPresenter::ApplicationPresenter(Application *application, Services &services,
                                           std::shared_ptr<IViewBuilder> &viewBuilder)
        : application(application) {
    view = viewBuilder->GetApplicationView(this);

    dashboardPresenter = std::make_shared<DashboardPresenter>(services, viewBuilder);
    rentPresenter = std::make_shared<RentPresenter>(application, services, viewBuilder);
    returnVehiclePresenter = std::make_shared<ReturnVehiclePresenter>(services, viewBuilder);
    vehiclesPresenter = std::make_shared<VehiclesPresenter>(application, services, viewBuilder);


    application->setOnDashboardStateChange([&] { DashboardView(); });
    application->setOnRentCarStateChange([&] { RentVehicleView(); });
    application->setOnReturnCarStateChange([&] { ReturnVehicleView(); });
    application->setOnVehiclesStateChange([&] { VehiclesView(); });
}

void ApplicationPresenter::Run() {
    if (view) {
        view->ShowDashboard();
        view->Run();
    }
}


void ApplicationPresenter::DashboardView() const {
    if (view) {
        view->ShowDashboard();
    }
}

void ApplicationPresenter::RentVehicleView() const {
    if (view) {
        view->ShowRentCar();
    }
}

void ApplicationPresenter::ReturnVehicleView() const {
    if (view) {
        view->ShowReturnCar();
    }
}

void ApplicationPresenter::VehiclesView() const {
    if (view) {
        view->ShowVehicles();
    }
}

void ApplicationPresenter::Dashboard() {
    application->handle(Event::Cancel{});
}

void ApplicationPresenter::RentVehicle() {
    application->handle(Event::RentSelect{});
}

void ApplicationPresenter::ReturnVehicle() {
    application->handle(Event::ReturnVehicleSelect{});
}

void ApplicationPresenter::Vehicles() {
    application->handle(Event::VehiclesSelect{});
}

