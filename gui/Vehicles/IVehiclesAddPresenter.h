//
// Created by Adamczyk Piotr on 04.01.2020.
//

#ifndef WSEICARRENTAL_IVEHICLESADDPRESENTER_H
#define WSEICARRENTAL_IVEHICLESADDPRESENTER_H

class IVehiclesAddPresenter {
public:
    virtual ~IVehiclesAddPresenter() = default;

    virtual void AddVehicle() = 0;

    virtual void Cancel() = 0;
};

#endif //WSEICARRENTAL_IVEHICLESADDPRESENTER_H
