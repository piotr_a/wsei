//
// Created by Adamczyk Piotr on 02.01.2020.
//

#include "VehiclesPresenter.h"
#include "VehiclesListDto.h"


VehiclesPresenter::VehiclesPresenter(Application *application, Services &services,
                                     std::shared_ptr<IViewBuilder> &viewBuilder)
        : application(application),
          vehicleService(services.Vehicle),
          rentService(services.Rent) {
    view = viewBuilder->GetVehiclesView(this);

    vehiclesAddPresenter = std::make_shared<VehiclesAddPresenter>(application, services, viewBuilder);

    UpdateView();
    vehicleObserver = vehicleService->Register([&] { UpdateView(); });
    rentObserver = rentService->Register([&] { UpdateView(); });
    application->vehiclesState.onVehiclesAddStateChange = [&](bool addMode) { VehiclesAddView(addMode); };
}

VehiclesPresenter::~VehiclesPresenter() {
    vehicleService->Remove(vehicleObserver);
    rentService->Remove(rentObserver);
}


void VehiclesPresenter::DeleteVehicle(int id) {
    if (rentService->IsVehicleRented(id)) {
        if (view) {
            view->ShowVehicleRented();
        }
        return;
    }
    vehicleService->DeleteVehicle(id);
}

void VehiclesPresenter::AddVehicle() {
    application->handle(Event::Vehicles::Add{});
}


void VehiclesPresenter::UpdateView() const {
    if (view) {
        auto vehicles = vehicleService->GetVehicles();
        std::vector<VehiclesListDto> vehiclesListDtos;
        vehiclesListDtos.reserve(vehicles.size());
        for (auto &&vehicle : vehicles) {
            vehiclesListDtos.push_back({vehicle.id, vehicle.plate, vehicle.type, vehicle.description, vehicle.price,
                                        rentService->IsVehicleRented(vehicle.id)});
        }
        view->UpdateVehicles(vehiclesListDtos);
    }
}

void VehiclesPresenter::VehiclesAddView(bool addMode) const {
    if (view) {
        view->VehiclesAdd(addMode);
    }
}