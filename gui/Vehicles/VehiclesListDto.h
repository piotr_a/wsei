//
// Created by Adamczyk Piotr on 06.01.2020.
//

#ifndef WSEICARRENTAL_VEHICLESLISTDTO_H
#define WSEICARRENTAL_VEHICLESLISTDTO_H

#include <string>

class VehiclesListDto {
public:
    int id;
    std::string plate;
    std::string type;
    std::string description;
    std::string price;
    bool rented;
};

#endif //WSEICARRENTAL_VEHICLESLISTDTO_H
