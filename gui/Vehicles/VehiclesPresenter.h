//
// Created by Adamczyk Piotr on 02.01.2020.
//

#ifndef WSEICARRENTAL_VEHICLESPRESENTER_H
#define WSEICARRENTAL_VEHICLESPRESENTER_H


#include <memory>
#include "IVehiclesView.h"
#include "../../domain/Application.h"
#include "../../domain/Services.h"
#include "../IViewBuilder.h"
#include "VehiclesAddPresenter.h"

class VehiclesPresenter : public IVehiclesPresenter {
    std::shared_ptr<IVehiclesView> view;
    Application *application;
    std::shared_ptr<VehicleService> vehicleService;
    std::shared_ptr<RentService> rentService;

    std::shared_ptr<VehiclesAddPresenter> vehiclesAddPresenter;
    VehicleService::Observer vehicleObserver;
    RentService::Observer rentObserver;
public:
    explicit VehiclesPresenter(Application *application, Services &services,
                               std::shared_ptr<IViewBuilder> &viewBuilder);

    ~VehiclesPresenter() override;

    void DeleteVehicle(int id) override;

    void AddVehicle() override;

    void UpdateView() const;

    void VehiclesAddView(bool addMode) const;
};

#endif //WSEICARRENTAL_VEHICLESPRESENTER_H
