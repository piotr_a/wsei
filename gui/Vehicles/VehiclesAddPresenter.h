//
// Created by Adamczyk Piotr on 06.12.2019.
//

#ifndef WSEICARRENTAL_VEHICLESADDPRESENTER_H
#define WSEICARRENTAL_VEHICLESADDPRESENTER_H


#include <memory>
#include "../../domain/VehicleService.h"
#include "IVehiclesAddView.h"
#include "../../domain/Application.h"
#include "IVehiclesAddPresenter.h"
#include "../../domain/Services.h"
#include "../IViewBuilder.h"

class VehiclesAddPresenter : public IVehiclesAddPresenter {
private:
    Application *application;
    std::shared_ptr<VehicleService> vehicleService;
    std::shared_ptr<IVehiclesAddView> view;
public:
    explicit VehiclesAddPresenter(Application *application, Services &services,
                                  std::shared_ptr<IViewBuilder> &viewBuilder);

    void AddVehicle() override;

    void Cancel() override;

    void Clear();

};


#endif //WSEICARRENTAL_VEHICLESADDPRESENTER_H
