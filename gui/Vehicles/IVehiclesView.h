//
// Created by Adamczyk Piotr on 02.01.2020.
//

#ifndef WSEICARRENTAL_IVEHICLESVIEW_H
#define WSEICARRENTAL_IVEHICLESVIEW_H

#include <vector>
#include "VehiclesListDto.h"


class IVehiclesView {
public:
    virtual ~IVehiclesView() = default;

    virtual void UpdateVehicles(const std::vector<VehiclesListDto> &vehicleDtos) = 0;

    virtual void VehiclesAdd(bool addMode) = 0;

    virtual void ShowVehicleRented() = 0;
};

#endif //WSEICARRENTAL_IVEHICLESVIEW_H
