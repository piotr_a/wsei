//
// Created by Adamczyk Piotr on 04.01.2020.
//

#ifndef WSEICARRENTAL_IVEHICLESPRESENTER_H
#define WSEICARRENTAL_IVEHICLESPRESENTER_H

class IVehiclesPresenter {
public:
    virtual ~IVehiclesPresenter() = default;

    virtual void DeleteVehicle(int id) = 0;

    virtual void AddVehicle() = 0;
};

#endif //WSEICARRENTAL_IVEHICLESPRESENTER_H
