//
// Created by Adamczyk Piotr on 06.12.2019.
//

#ifndef WSEICARRENTAL_IVEHICLESADDVIEW_H
#define WSEICARRENTAL_IVEHICLESADDVIEW_H


#include <string>

class IVehiclesAddView {
public:
    virtual ~IVehiclesAddView() = default;

    virtual std::string getPlate() = 0;

    virtual std::string getType() = 0;

    virtual std::string getDescription() = 0;

    virtual std::string getPrice() = 0;

    virtual void clear() = 0;
};

#endif //WSEICARRENTAL_IVEHICLESADDVIEW_H
