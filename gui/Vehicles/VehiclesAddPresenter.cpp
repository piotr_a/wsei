//
// Created by Adamczyk Piotr on 06.12.2019.
//

#include "VehiclesAddPresenter.h"
#include <algorithm>


VehiclesAddPresenter::VehiclesAddPresenter(Application *application, Services &services,
                                           std::shared_ptr<IViewBuilder> &viewBuilder)
        : application(application),
          vehicleService(services.Vehicle) {
    view = viewBuilder->GetVehiclesAddView(this);
    Clear();
}


void VehiclesAddPresenter::AddVehicle() {
    if (view) {
        auto price = view->getPrice();
        if (!(!price.empty() &&
              std::find_if(price.begin(), price.end(),
                           [](unsigned char c) { return !(std::isdigit(c) || c == ','); }) ==
              price.end())) {
            return;
        }

        int id = vehicleService->AddVehicle(
                view->getPlate(),
                view->getType(),
                view->getType(),
                view->getPrice());
        if (id > 0) {
            Clear();
            application->handle(Event::Vehicles::AddAdd{});
        }
    }
}

void VehiclesAddPresenter::Cancel() {
    Clear();
    application->handle(Event::Vehicles::AddCancel{});

}

void VehiclesAddPresenter::Clear() {
    if (view) {
        view->clear();
    }
}