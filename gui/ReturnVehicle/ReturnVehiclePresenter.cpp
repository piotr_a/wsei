//
// Created by Adamczyk Piotr on 18.12.2019.
//

#include "ReturnVehiclePresenter.h"

ReturnVehiclePresenter::ReturnVehiclePresenter(Services &services, std::shared_ptr<IViewBuilder> &viewBuilder)
        :
        rentService(services.Rent) {
    view = viewBuilder->GetReturnVehicleView(this);
    UpdateView();
    observer = rentService->Register([&] { UpdateView(); });
}

ReturnVehiclePresenter::~ReturnVehiclePresenter() {
    rentService->Remove(observer);
}


void ReturnVehiclePresenter::ReturnVehicle(int id) {
    if (auto rent = rentService->GetRent(id)) {
        if (view && view->ShowPay(rent->CalculatePay())) {
            rentService->ReturnRent(id);
        }
    }
}

void ReturnVehiclePresenter::UpdateView() const {
    if (view) {
        view->UpdateRents(rentService->GetActiveRents());
    }
}