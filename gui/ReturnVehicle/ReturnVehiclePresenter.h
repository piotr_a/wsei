//
// Created by Adamczyk Piotr on 18.12.2019.
//

#ifndef WSEICARRENTAL_RETURNVEHICLEPRESENTER_H
#define WSEICARRENTAL_RETURNVEHICLEPRESENTER_H


#include <memory>
#include "IReturnVehicleView.h"
#include "../../domain/Services.h"
#include "../IViewBuilder.h"
#include "IReturnVehiclePresenter.h"

class ReturnVehiclePresenter : public IReturnVehiclePresenter {
private:
    std::shared_ptr<RentService> rentService;
    std::shared_ptr<IReturnVehicleView> view;

    RentService::Observer observer;
public:
    explicit ReturnVehiclePresenter(Services &services, std::shared_ptr<IViewBuilder> &viewBuilder);

    ~ReturnVehiclePresenter() override;

    void ReturnVehicle(int id) override;

    void UpdateView() const;
};


#endif //WSEICARRENTAL_RETURNVEHICLEPRESENTER_H
