//
// Created by Adamczyk Piotr on 18.12.2019.
//

#ifndef WSEICARRENTAL_IRETURNVEHICLEVIEW_H
#define WSEICARRENTAL_IRETURNVEHICLEVIEW_H


#include <vector>
#include "../../domain/RentDto.h"

class IReturnVehicleView {
public:
    virtual ~IReturnVehicleView() = default;

    virtual void UpdateRents(const std::vector<RentDto> &rentDtos) = 0;

    virtual bool ShowPay(const std::string &pay) = 0;
};


#endif //WSEICARRENTAL_IRETURNVEHICLEVIEW_H
