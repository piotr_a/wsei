//
// Created by Adamczyk Piotr on 04.01.2020.
//

#ifndef WSEICARRENTAL_IRETURNVEHICLEPRESENTER_H
#define WSEICARRENTAL_IRETURNVEHICLEPRESENTER_H

class IReturnVehiclePresenter {
public:
    virtual ~IReturnVehiclePresenter() = default;

    virtual void ReturnVehicle(int id) = 0;
};


#endif //WSEICARRENTAL_IRETURNVEHICLEPRESENTER_H
