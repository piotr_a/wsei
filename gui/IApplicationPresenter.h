//
// Created by Adamczyk Piotr on 04.01.2020.
//

#ifndef WSEICARRENTAL_IAPPLICATIONPRESENTER_H
#define WSEICARRENTAL_IAPPLICATIONPRESENTER_H

#include "IApplicationView.h"

class IApplicationPresenter {
public:
    virtual ~IApplicationPresenter() = default;

    virtual void Dashboard() = 0;

    virtual void RentVehicle() = 0;

    virtual void ReturnVehicle() = 0;

    virtual void Vehicles() = 0;
};

#endif //WSEICARRENTAL_IAPPLICATIONPRESENTER_H
