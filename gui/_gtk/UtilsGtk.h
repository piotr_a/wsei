//
// Created by Adamczyk Piotr on 09.12.2019.
//

#ifndef WSEICARRENTAL_UTILSGTK_H
#define WSEICARRENTAL_UTILSGTK_H


#include <string>
#include <gtkmm-3.0\gtkmm.h>

class UtilsGtk {
public:

    template<class Tcomponent, class Treturn, class Tobject>
    static void setSignal(const Glib::RefPtr<Gtk::Builder> &builder, const std::string &componentName, Tobject &object,
                          Treturn (Tobject::*method)()) {
        Tcomponent *component = nullptr;
        builder->get_widget(componentName, component);
        if (component) {
            component->signal_clicked().connect(sigc::mem_fun(object, method));
        }

    }

    template<class Tcomponent, class Treturn, class Tobject>
    static void
    setSignalToggled(const Glib::RefPtr<Gtk::Builder> &builder, const std::string &componentName, Tobject &object,
                     Treturn (Tobject::*method)()) {
        Tcomponent *component = nullptr;
        builder->get_widget(componentName, component);
        if (component) {
            component->signal_toggled().connect(sigc::mem_fun(object, method));
        }

    }

    template<class Tcomponent>
    static std::string getText(const Glib::RefPtr<Gtk::Builder> &builder, const std::string &componentName) {
        Tcomponent *component = nullptr;
        builder->get_widget(componentName, component);
        if (!component) {
            return "";
        }
        return component->get_text();
    }

    template<class Tcomponent>
    static void
    setText(const Glib::RefPtr<Gtk::Builder> &builder, const std::string &componentName, const std::string &text) {
        Tcomponent *component = nullptr;
        builder->get_widget(componentName, component);
        if (component) {
            component->set_text(text);
        }
    }

    template<class Tcomponent>
    static void
    setPage(const Glib::RefPtr<Gtk::Builder> &builder, const std::string &componentName, const std::string &pageName) {
        Glib::RefPtr<Tcomponent> component = Glib::RefPtr<Tcomponent>::cast_dynamic(builder->get_object(componentName));
        if (component) {
            component->set_visible_child(pageName);
        }
    }

    template<class Tcomponent>
    static void
    setActive(const Glib::RefPtr<Gtk::Builder> &builder, const std::string &componentName, const bool active) {
        Tcomponent *component = nullptr;
        builder->get_widget(componentName, component);
        if (component) {
            component->set_active(active);
        }
    }

    template<class Tcomponent>
    static bool getActive(const Glib::RefPtr<Gtk::Builder> &builder, const std::string &componentName) {
        Tcomponent *component = nullptr;
        builder->get_widget(componentName, component);
        if (!component) {
            return false;
        }
        return component->get_active();
    }


};


#endif //WSEICARRENTAL_UTILSGTK_H
