//
// Created by Adamczyk Piotr on 05.12.2019.
//

#ifndef WSEICARRENTAL_APPLICATIONVIEWGTK_H
#define WSEICARRENTAL_APPLICATIONVIEWGTK_H


#include <memory>
#include <gtkmm-3.0\gtkmm.h>
#include "../IApplicationView.h"
#include "../IApplicationPresenter.h"

class ApplicationViewGtk : public IApplicationView {
    IApplicationPresenter *presenter;
    Glib::RefPtr<Gtk::Builder> builder;

    bool freeze_notify;

    void setButton(bool dashboard, bool rentVehicle, bool returnVehicle, bool vehicles);

    void onDashboardClick();

    void onRentVehicleClick();

    void onReturnVehicleClick();

    void onVehiclesClick();

public:
    explicit ApplicationViewGtk(IApplicationPresenter *presenter, const Glib::RefPtr<Gtk::Builder> &builder);

    void Run() override;

    void ShowDashboard() override;

    void ShowRentCar() override;

    void ShowReturnCar() override;

    void ShowVehicles() override;
};


#endif //WSEICARRENTAL_APPLICATIONVIEWGTK_H
