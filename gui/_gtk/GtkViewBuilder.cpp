//
// Created by Adamczyk Piotr on 04.01.2020.
//

#include "GtkViewBuilder.h"
#include "ApplicationViewGtk.h"
#include "Dashboard/DashboardViewGtk.h"
#include "Rent/RentViewGtk.h"
#include "ReturnVehicle/ReturnVehicleViewGtk.h"
#include "Vehicles/VehiclesViewGtk.h"
#include "Rent/RentVehicleListViewGtk.h"
#include "Rent/RentCustomerListViewGtk.h"
#include "Rent/RentAddCustomerViewGtk.h"
#include "Rent/RentSummaryViewGtk.h"
#include "Vehicles/VehiclesAddViewGtk.h"

GtkViewBuilder::GtkViewBuilder() {
    application = Gtk::Application::create();
    builder = Gtk::Builder::create_from_file("gui.glade");
}

std::shared_ptr<IApplicationView> GtkViewBuilder::GetApplicationView(IApplicationPresenter *applicationPresenter) {
    return std::make_shared<ApplicationViewGtk>(applicationPresenter, builder);
}

std::shared_ptr<IDashboardView> GtkViewBuilder::GetDashboardView(IDashboardPresenter *presenter) {
    return std::make_shared<DashboardViewGtk>(presenter, builder);
}

std::shared_ptr<IRentView> GtkViewBuilder::GetRentView(IRentPresenter *presenter) {
    return std::make_shared<RentViewGtk>(presenter, builder);
}

std::shared_ptr<IReturnVehicleView> GtkViewBuilder::GetReturnVehicleView(IReturnVehiclePresenter *presenter) {
    return std::make_shared<ReturnVehicleViewGtk>(presenter, builder);
}

std::shared_ptr<IVehiclesView> GtkViewBuilder::GetVehiclesView(IVehiclesPresenter *presenter) {
    return std::make_shared<VehiclesViewGtk>(presenter, builder);
}

std::shared_ptr<IRentVehicleListView> GtkViewBuilder::GetRentVehicleListView(IRentVehicleListPresenter *presenter) {
    return std::make_shared<RentVehicleListViewGtk>(presenter, builder);
}

std::shared_ptr<IRentCustomerListView> GtkViewBuilder::GetRentCustomerListView(IRentCustomerListPresenter *presenter) {
    return std::make_shared<RentCustomerListViewGtk>(presenter, builder);
}

std::shared_ptr<IRentAddCustomerView> GtkViewBuilder::GetRentAddCustomerView(IRentAddCustomerPresenter *presenter) {
    return std::make_shared<RentAddCustomerViewGtk>(presenter, builder);
}

std::shared_ptr<IRentSummaryView> GtkViewBuilder::GetRentSummaryView(IRentSummaryPresenter *presenter) {
    return std::make_shared<RentSummaryViewGtk>(presenter, builder);
}

std::shared_ptr<IVehiclesAddView> GtkViewBuilder::GetVehiclesAddView(IVehiclesAddPresenter *presenter) {
    return std::make_shared<VehiclesAddViewGtk>(presenter, builder);
}