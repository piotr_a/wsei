//
// Created by Adamczyk Piotr on 18.12.2019.
//

#ifndef WSEICARRENTAL_RETURNVEHICLEVIEWGTK_H
#define WSEICARRENTAL_RETURNVEHICLEVIEWGTK_H


#include "../../ReturnVehicle/IReturnVehicleView.h"
#include "../../../domain/RentDto.h"
#include "../../ReturnVehicle/ReturnVehiclePresenter.h"
#include "../../ReturnVehicle/IReturnVehiclePresenter.h"
#include <gtkmm-3.0\gtkmm.h>

class ReturnVehicleViewGtk : public IReturnVehicleView {
    IReturnVehiclePresenter *presenter;
    Glib::RefPtr<Gtk::Builder> builder;

    void onReturnClick();

public:
    ReturnVehicleViewGtk(IReturnVehiclePresenter *presenter,
                         const Glib::RefPtr<Gtk::Builder> &builder);

    void UpdateRents(const std::vector<RentDto> &rentDtos) override;

    bool ShowPay(const std::string &pay) override;

};


#endif //WSEICARRENTAL_RETURNVEHICLEVIEWGTK_H
