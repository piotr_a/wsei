//
// Created by Adamczyk Piotr on 18.12.2019.
//

#include "ReturnVehicleViewGtk.h"
#include "../UtilsGtk.h"
#include <glibmm/main.h>

class RentListModel : public Gtk::TreeModel::ColumnRecord {
public:
    RentListModel() {
        this->add(this->id);
        this->add(this->plate);
        this->add(this->name);
        this->add(this->surname);
        this->add(this->rentFrom);
    }

    Gtk::TreeModelColumn<gint> id;
    Gtk::TreeModelColumn<Glib::ustring> plate;
    Gtk::TreeModelColumn<Glib::ustring> name;
    Gtk::TreeModelColumn<Glib::ustring> surname;
    Gtk::TreeModelColumn<Glib::ustring> rentFrom;
};

ReturnVehicleViewGtk::ReturnVehicleViewGtk(IReturnVehiclePresenter *presenter,
                                           const Glib::RefPtr<Gtk::Builder> &builder)
        : presenter(presenter),
          builder(builder) {

    UtilsGtk::setSignal<Gtk::ToolButton>(builder, "ReturnVehicleReturn", *this,
                                         &ReturnVehicleViewGtk::onReturnClick);
}

void ReturnVehicleViewGtk::UpdateRents(const std::vector<RentDto> &rentDtos) {

    auto rentStore = Glib::RefPtr<Gtk::ListStore>::cast_dynamic(builder->get_object("ReturnVehicleListStore"));
    rentStore->freeze_notify();
    rentStore->clear();

    RentListModel model;

    for (auto &&rentDto : rentDtos) {
        auto row = *(rentStore->append());
        row[model.id] = rentDto.id;
        row[model.plate] = rentDto.plate;
        row[model.name] = rentDto.name;
        row[model.surname] = rentDto.surname;
        row[model.rentFrom] = rentDto.rentFrom;
    }
    rentStore->thaw_notify();
}

bool ReturnVehicleViewGtk::ShowPay(const std::string &pay) {
    Gtk::Window *component = nullptr;
    builder->get_widget("MainWindow", component);
    Gtk::MessageDialog dialog(*component, "To pay: " + pay, false, Gtk::MESSAGE_INFO, Gtk::BUTTONS_OK_CANCEL);

    return dialog.run() == Gtk::RESPONSE_OK;
}

void ReturnVehicleViewGtk::onReturnClick() {
    auto rentList = Glib::RefPtr<Gtk::TreeView>::cast_dynamic(builder->get_object("ReturnVehicleList"));
    auto rentListSelected = rentList->get_selection();
    Gtk::TreeModel::Row row = *rentListSelected->get_selected();
    if (row) {
        RentListModel model;
        int id = row[model.id];

        if (presenter) {
            presenter->ReturnVehicle(id);
        }
    }
}