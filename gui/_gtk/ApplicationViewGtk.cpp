//
// Created by Adamczyk Piotr on 05.12.2019.
//

#include "ApplicationViewGtk.h"
#include "UtilsGtk.h"

ApplicationViewGtk::ApplicationViewGtk(IApplicationPresenter *presenter, const Glib::RefPtr<Gtk::Builder> &builder)
        : presenter(presenter),
          builder(builder),
          freeze_notify(false) {

    UtilsGtk::setSignal<Gtk::ToggleToolButton>(builder, "Dashboard", *this, &ApplicationViewGtk::onDashboardClick);
    UtilsGtk::setSignal<Gtk::ToggleToolButton>(builder, "RentVehicle", *this, &ApplicationViewGtk::onRentVehicleClick);
    UtilsGtk::setSignal<Gtk::ToggleToolButton>(builder, "ReturnVehicle", *this,
                                               &ApplicationViewGtk::onReturnVehicleClick);
    UtilsGtk::setSignal<Gtk::ToggleToolButton>(builder, "Vehicles", *this, &ApplicationViewGtk::onVehiclesClick);
}

void ApplicationViewGtk::Run() {
    auto application = Gtk::Application::create();
    Gtk::Window *window = nullptr;
    builder->get_widget("MainWindow", window);
    application->run(*window);
}

void ApplicationViewGtk::ShowDashboard() {
    UtilsGtk::setPage<Gtk::Stack>(builder, "PageControl", "DashboardPage");
    setButton(true, false, false, false);
}

void ApplicationViewGtk::ShowRentCar() {
    UtilsGtk::setPage<Gtk::Stack>(builder, "PageControl", "RentCarPage");
    setButton(false, true, false, false);
}

void ApplicationViewGtk::ShowReturnCar() {
    UtilsGtk::setPage<Gtk::Stack>(builder, "PageControl", "ReturnCarPage");
    setButton(false, false, true, false);
}

void ApplicationViewGtk::ShowVehicles() {
    UtilsGtk::setPage<Gtk::Stack>(builder, "PageControl", "VehiclesPage");
    setButton(false, false, false, true);
}

void ApplicationViewGtk::setButton(bool dashboard, bool rentVehicle, bool returnVehicle, bool vehicles) {
    freeze_notify = true;
    UtilsGtk::setActive<Gtk::ToggleToolButton>(builder, "Dashboard", dashboard);
    UtilsGtk::setActive<Gtk::ToggleToolButton>(builder, "RentVehicle", rentVehicle);
    UtilsGtk::setActive<Gtk::ToggleToolButton>(builder, "ReturnVehicle", returnVehicle);
    UtilsGtk::setActive<Gtk::ToggleToolButton>(builder, "Vehicles", vehicles);
    freeze_notify = false;
}

void ApplicationViewGtk::onDashboardClick() {
    if (freeze_notify) return;
    if (presenter) {
        presenter->Dashboard();
    }
    setButton(true, false, false, false);
}

void ApplicationViewGtk::onRentVehicleClick() {
    if (freeze_notify) return;
    if (presenter) {
        presenter->RentVehicle();
    }
    setButton(false, true, false, false);
}

void ApplicationViewGtk::onReturnVehicleClick() {
    if (freeze_notify) return;
    if (presenter) {
        presenter->ReturnVehicle();
    }
    setButton(false, false, true, false);
}

void ApplicationViewGtk::onVehiclesClick() {
    if (freeze_notify) return;
    if (presenter) {
        presenter->Vehicles();
    }
    setButton(false, false, false, true);
}
