//
// Created by Adamczyk Piotr on 06.12.2019.
//

#include "VehiclesAddViewGtk.h"
#include "../UtilsGtk.h"


VehiclesAddViewGtk::VehiclesAddViewGtk(IVehiclesAddPresenter *presenter,
                                       const Glib::RefPtr<Gtk::Builder> &builder)
        : presenter(presenter),
          builder(builder) {
    UtilsGtk::setSignal<Gtk::ToolButton>(builder, "AddVehicleAdd", *this,
                                         &VehiclesAddViewGtk::onAddClick);
    UtilsGtk::setSignal<Gtk::ToolButton>(builder, "AddVehicleCancel", *this,
                                         &VehiclesAddViewGtk::onCancelClick);
}

std::string VehiclesAddViewGtk::getPlate() {
    return UtilsGtk::getText<Gtk::Entry>(builder, "AddVehiclePlate");
}

std::string VehiclesAddViewGtk::getType() {
    return UtilsGtk::getText<Gtk::Entry>(builder, "AddVehicleType");
}

std::string VehiclesAddViewGtk::getDescription() {
    return UtilsGtk::getText<Gtk::Entry>(builder, "AddVehicleDescription");
}

std::string VehiclesAddViewGtk::getPrice() {
    return UtilsGtk::getText<Gtk::Entry>(builder, "AddVehiclePrice");
}

void VehiclesAddViewGtk::clear() {
    UtilsGtk::setText<Gtk::Entry>(builder, "AddVehiclePlate", "");
    UtilsGtk::setText<Gtk::Entry>(builder, "AddVehicleType", "");
    UtilsGtk::setText<Gtk::Entry>(builder, "AddVehicleDescription", "");
    UtilsGtk::setText<Gtk::Entry>(builder, "AddVehiclePrice", "");
}

void VehiclesAddViewGtk::onAddClick() {
    if (presenter) {
        presenter->AddVehicle();
    }
}

void VehiclesAddViewGtk::onCancelClick() {
    if (presenter) {
        presenter->Cancel();
    }
}
