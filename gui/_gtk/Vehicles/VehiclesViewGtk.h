//
// Created by Adamczyk Piotr on 02.01.2020.
//

#ifndef WSEICARRENTAL_VEHICLESVIEWGTK_H
#define WSEICARRENTAL_VEHICLESVIEWGTK_H

#include <gtkmm-3.0\gtkmm.h>
#include "../../Vehicles/VehiclesPresenter.h"
#include "../../Vehicles/IVehiclesView.h"

class VehiclesViewGtk : public IVehiclesView {
    IVehiclesPresenter *presenter;
    Glib::RefPtr<Gtk::Builder> builder;

    void onDeleteClick();

    void onAddClick();

public:
    VehiclesViewGtk(IVehiclesPresenter *presenter,
                    const Glib::RefPtr<Gtk::Builder> &builder);

    void UpdateVehicles(const std::vector<VehiclesListDto> &vehicleDtos) override;

    void VehiclesAdd(bool addMode) override;

    void ShowVehicleRented() override;
};


#endif //WSEICARRENTAL_VEHICLESVIEWGTK_H
