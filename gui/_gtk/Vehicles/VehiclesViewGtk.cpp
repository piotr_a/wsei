//
// Created by Adamczyk Piotr on 02.01.2020.
//

#include "VehiclesViewGtk.h"


#include "../UtilsGtk.h"


class VehiclesListModel : public Gtk::TreeModel::ColumnRecord {
public:
    VehiclesListModel() {
        // This order must match the column order in the .glade file
        this->add(this->id);
        this->add(this->plate);
        this->add(this->type);
        this->add(this->description);
        this->add(this->price);
        this->add(this->rented);
    }

    // These types must match those for the model in the .glade file
    Gtk::TreeModelColumn<gint> id;
    Gtk::TreeModelColumn<Glib::ustring> plate;
    Gtk::TreeModelColumn<Glib::ustring> type;
    Gtk::TreeModelColumn<Glib::ustring> description;
    Gtk::TreeModelColumn<Glib::ustring> price;
    Gtk::TreeModelColumn<Glib::ustring> rented;
};


VehiclesViewGtk::VehiclesViewGtk(IVehiclesPresenter *presenter,
                                 const Glib::RefPtr<Gtk::Builder> &builder)
        : presenter(presenter),
          builder(builder) {
    UtilsGtk::setSignal<Gtk::ToolButton>(builder, "VehiclesDelete", *this,
                                         &VehiclesViewGtk::onDeleteClick);
    UtilsGtk::setSignal<Gtk::ToolButton>(builder, "VehiclesAdd", *this,
                                         &VehiclesViewGtk::onAddClick);
}

void VehiclesViewGtk::UpdateVehicles(const std::vector<VehiclesListDto> &vehicleDtos) {

    auto vehicleStore = Glib::RefPtr<Gtk::ListStore>::cast_dynamic(builder->get_object("VehiclesListStore"));
    vehicleStore->freeze_notify();
    vehicleStore->clear();

    VehiclesListModel model;

    for (auto &&vehicleDto : vehicleDtos) {
        auto row = *(vehicleStore->append());
        row[model.id] = vehicleDto.id;
        row[model.plate] = vehicleDto.plate;
        row[model.type] = vehicleDto.type;
        row[model.description] = vehicleDto.description;
        row[model.price] = vehicleDto.price;
        row[model.rented] = (vehicleDto.rented) ? "Yes" : "No";
    }
    vehicleStore->thaw_notify();
}

void VehiclesViewGtk::VehiclesAdd(const bool addMode) {
    UtilsGtk::setPage<Gtk::Stack>(builder, "VehiclesPageControl", (addMode) ? "VehiclesAddPage" : "VehiclesListPage");
}

void VehiclesViewGtk::ShowVehicleRented() {
    Gtk::Window *component = nullptr;
    builder->get_widget("MainWindow", component);
    Gtk::MessageDialog dialog(*component, "A rented vehicle cannot be deleted.", false, Gtk::MESSAGE_ERROR,
                              Gtk::BUTTONS_OK);
    dialog.run();
}

void VehiclesViewGtk::onDeleteClick() {
    auto vehicleList = Glib::RefPtr<Gtk::TreeView>::cast_dynamic(builder->get_object("VehiclesList"));
    auto vehicleListSelected = vehicleList->get_selection();
    Gtk::TreeModel::Row row = *vehicleListSelected->get_selected();
    if (row) {
        VehiclesListModel model;
        int id = row[model.id];

        if (presenter) {
            presenter->DeleteVehicle(id);
        }
    }
}

void VehiclesViewGtk::onAddClick() {
    if (presenter) {
        presenter->AddVehicle();
    }
}