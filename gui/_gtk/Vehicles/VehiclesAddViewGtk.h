//
// Created by Adamczyk Piotr on 06.12.2019.
//

#ifndef WSEICARRENTAL_VEHICLESADDVIEWGTK_H
#define WSEICARRENTAL_VEHICLESADDVIEWGTK_H

#include <gtkmm-3.0\gtkmm.h>
#include "../../Vehicles/IVehiclesAddView.h"
#include "../../Vehicles/VehiclesAddPresenter.h"

class VehiclesAddViewGtk : public IVehiclesAddView {
private:
    IVehiclesAddPresenter *presenter;
    Glib::RefPtr<Gtk::Builder> builder;

    void onAddClick();

    void onCancelClick();

public:
    VehiclesAddViewGtk(IVehiclesAddPresenter *presenter,
                       const Glib::RefPtr<Gtk::Builder> &builder);

    std::string getPlate() override;

    std::string getType() override;

    std::string getDescription() override;

    std::string getPrice() override;

    void clear() override;
};


#endif //WSEICARRENTAL_VEHICLESADDVIEWGTK_H
