//
// Created by Adamczyk Piotr on 02.01.2020.
//

#include "DashboardViewGtk.h"
#include "../UtilsGtk.h"

DashboardViewGtk::DashboardViewGtk(IDashboardPresenter *presenter,
                                   Glib::RefPtr<Gtk::Builder> &builder)
        : presenter(presenter),
          builder(builder) {
}


void DashboardViewGtk::Update(const int count, int toRentCount) {
    UtilsGtk::setText<Gtk::Label>(builder, "DashboardRentCountLabel", std::to_string(count));
    UtilsGtk::setText<Gtk::Label>(builder, "DashboardToRentCountLabel", std::to_string(toRentCount));
}