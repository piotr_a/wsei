//
// Created by Adamczyk Piotr on 02.01.2020.
//

#ifndef WSEICARRENTAL_DASHBOARDVIEWGTK_H
#define WSEICARRENTAL_DASHBOARDVIEWGTK_H

#include <memory>
#include "../../Dashboard/IDashboardView.h"
#include "../../Dashboard/IDashboardPresenter.h"
#include <gtkmm-3.0\gtkmm.h>

class DashboardViewGtk : public IDashboardView {
    IDashboardPresenter *presenter;
    Glib::RefPtr<Gtk::Builder> builder;

public:
    DashboardViewGtk(IDashboardPresenter *presenter,
                     Glib::RefPtr<Gtk::Builder> &builder);

    void Update(int count, int toRentCount) override;

};


#endif //WSEICARRENTAL_DASHBOARDVIEWGTK_H
