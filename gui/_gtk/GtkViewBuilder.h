//
// Created by Adamczyk Piotr on 04.01.2020.
//

#ifndef WSEICARRENTAL_GTKVIEWBUILDER_H
#define WSEICARRENTAL_GTKVIEWBUILDER_H


#include "../IViewBuilder.h"
#include <gtkmm-3.0\gtkmm.h>

class GtkViewBuilder : public IViewBuilder {
private:
    Glib::RefPtr<Gtk::Application> application;
    Glib::RefPtr<Gtk::Builder> builder;
public:
    GtkViewBuilder();

    std::shared_ptr<IApplicationView> GetApplicationView(IApplicationPresenter *presenter) override;

    std::shared_ptr<IDashboardView> GetDashboardView(IDashboardPresenter *presenter) override;

    std::shared_ptr<IRentView> GetRentView(IRentPresenter *presenter) override;

    std::shared_ptr<IReturnVehicleView> GetReturnVehicleView(IReturnVehiclePresenter *presenter) override;

    std::shared_ptr<IVehiclesView> GetVehiclesView(IVehiclesPresenter *presenter) override;

    std::shared_ptr<IRentVehicleListView> GetRentVehicleListView(IRentVehicleListPresenter *presenter) override;

    std::shared_ptr<IRentCustomerListView> GetRentCustomerListView(IRentCustomerListPresenter *presenter) override;

    std::shared_ptr<IRentAddCustomerView> GetRentAddCustomerView(IRentAddCustomerPresenter *presenter) override;

    std::shared_ptr<IRentSummaryView> GetRentSummaryView(IRentSummaryPresenter *presenter) override;

    std::shared_ptr<IVehiclesAddView> GetVehiclesAddView(IVehiclesAddPresenter *presenter) override;
};


#endif //WSEICARRENTAL_GTKVIEWBUILDER_H
