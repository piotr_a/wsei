//
// Created by Adamczyk Piotr on 04.01.2020.
//

#ifndef WSEICARRENTAL_RENTVIEWGTK_H
#define WSEICARRENTAL_RENTVIEWGTK_H


#include <memory>
#include "../../Rent/IRentView.h"
#include "../../Rent/IRentPresenter.h"
#include <gtkmm-3.0\gtkmm.h>

class RentViewGtk : public IRentView {
private:
    IRentPresenter *presenter;
    Glib::RefPtr<Gtk::Builder> builder;

public:
    RentViewGtk(IRentPresenter *presenter,
                const Glib::RefPtr<Gtk::Builder> &builder);

    void ShowVehicleList() override;

    void ShowCustomerList() override;

    void ShowCustomerAdd() override;

    void ShowRentSummary() override;
};


#endif //WSEICARRENTAL_RENTVIEWGTK_H
