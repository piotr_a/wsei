//
// Created by Adamczyk Piotr on 06.12.2019.
//

#ifndef WSEICARRENTAL_RENTCUSTOMERLISTVIEWGTK_H
#define WSEICARRENTAL_RENTCUSTOMERLISTVIEWGTK_H


#include "../../Rent/RentCustomerListPresenter.h"
#include <gtkmm-3.0\gtkmm.h>

class RentCustomerListViewGtk : public IRentCustomerListView {
    IRentCustomerListPresenter *presenter;
    Glib::RefPtr<Gtk::Builder> builder;

    void onSelectClick();

    void onDeleteClick();

    void onAddClick();

public:
    RentCustomerListViewGtk(IRentCustomerListPresenter *presenter,
                            const Glib::RefPtr<Gtk::Builder> &builder);

    void UpdateCustomers(const std::vector<CustomerDto> &customerDtos) override;

};


#endif //WSEICARRENTAL_RENTCUSTOMERLISTVIEWGTK_H
