//
// Created by Adamczyk Piotr on 04.12.2019.
//

#ifndef WSEICARRENTAL_RENTVEHICLELISTVIEWGTK_H
#define WSEICARRENTAL_RENTVEHICLELISTVIEWGTK_H


#include <memory>
#include <gtkmm-3.0\gtkmm.h>
#include "../../Rent/IRentVehicleListView.h"
#include "../../Rent/IRentVehicleListPresenter.h"

class RentVehicleListViewGtk : public IRentVehicleListView {
    IRentVehicleListPresenter *presenter;
    Glib::RefPtr<Gtk::Builder> builder;

    void onSelectClick();

public:
    RentVehicleListViewGtk(IRentVehicleListPresenter *presenter,
                           const Glib::RefPtr<Gtk::Builder> &builder);

    void UpdateVehicles(const std::vector<VehicleDto> &vehicleDtos) override;

};


#endif //WSEICARRENTAL_RENTVEHICLELISTVIEWGTK_H
