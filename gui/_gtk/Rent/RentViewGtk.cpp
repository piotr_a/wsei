//
// Created by Adamczyk Piotr on 04.01.2020.
//
#include "RentViewGtk.h"
#include "../UtilsGtk.h"


RentViewGtk::RentViewGtk(IRentPresenter *presenter, const Glib::RefPtr<Gtk::Builder> &builder)
        : presenter(presenter),
          builder(builder) {

}

void RentViewGtk::ShowVehicleList() {
    UtilsGtk::setPage<Gtk::Stack>(builder, "RentVehiclePageControl", "RentVehicleListPage");
}

void RentViewGtk::ShowCustomerList() {
    UtilsGtk::setPage<Gtk::Stack>(builder, "RentVehiclePageControl", "RentCustomerListPage");
}

void RentViewGtk::ShowCustomerAdd() {
    UtilsGtk::setPage<Gtk::Stack>(builder, "RentVehiclePageControl", "RentAddCustomerPage");
}


void RentViewGtk::ShowRentSummary() {
    UtilsGtk::setPage<Gtk::Stack>(builder, "RentVehiclePageControl", "RentSummaryPage");
}
