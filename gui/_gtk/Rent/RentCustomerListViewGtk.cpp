//
// Created by Adamczyk Piotr on 06.12.2019.
//

#include "RentCustomerListViewGtk.h"
#include "../UtilsGtk.h"

class CustomerListModel : public Gtk::TreeModel::ColumnRecord {
public:
    CustomerListModel() {
        this->add(this->Id);
        this->add(this->Name);
        this->add(this->Surname);
        this->add(this->Address);
        this->add(this->PhoneNumber);
    }

    Gtk::TreeModelColumn<gint> Id;
    Gtk::TreeModelColumn<Glib::ustring> Name;
    Gtk::TreeModelColumn<Glib::ustring> Surname;
    Gtk::TreeModelColumn<Glib::ustring> Address;
    Gtk::TreeModelColumn<Glib::ustring> PhoneNumber;
};

RentCustomerListViewGtk::RentCustomerListViewGtk(IRentCustomerListPresenter *presenter,
                                                 const Glib::RefPtr<Gtk::Builder> &builder)
        : presenter(presenter),
          builder(builder) {

    UtilsGtk::setSignal<Gtk::ToolButton>(builder, "CustomerListSelect", *this,
                                         &RentCustomerListViewGtk::onSelectClick);
    UtilsGtk::setSignal<Gtk::ToolButton>(builder, "CustomerListDelete", *this,
                                         &RentCustomerListViewGtk::onDeleteClick);

    UtilsGtk::setSignal<Gtk::ToolButton>(builder, "CustomerListAdd", *this,
                                         &RentCustomerListViewGtk::onAddClick);

}

void RentCustomerListViewGtk::UpdateCustomers(const std::vector<CustomerDto> &customerDtos) {

    auto customerStore = Glib::RefPtr<Gtk::ListStore>::cast_dynamic(builder->get_object("RentCustomerListStore"));
    customerStore->freeze_notify();
    customerStore->clear();

    CustomerListModel model;

    for (auto &&customerDto : customerDtos) {
        auto row = *(customerStore->append());
        row[model.Id] = customerDto.Id;
        row[model.Name] = customerDto.Name;
        row[model.Surname] = customerDto.Surname;
        row[model.Address] = customerDto.Address;
        row[model.PhoneNumber] = customerDto.PhoneNumber;
    }
    customerStore->thaw_notify();
}

void RentCustomerListViewGtk::onSelectClick() {
    auto customerList = Glib::RefPtr<Gtk::TreeView>::cast_dynamic(builder->get_object("RentCustomerList"));
    auto customerListSelected = customerList->get_selection();
    Gtk::TreeModel::Row row = *customerListSelected->get_selected();
    if (row) {
        CustomerListModel model;
        int id = row[model.Id];

        if (presenter) {
            presenter->SelectCustomer(id);
        }
    }
}

void RentCustomerListViewGtk::onDeleteClick() {
    auto customerList = Glib::RefPtr<Gtk::TreeView>::cast_dynamic(builder->get_object("RentCustomerList"));
    auto customerListSelected = customerList->get_selection();
    Gtk::TreeModel::Row row = *customerListSelected->get_selected();
    if (row) {
        CustomerListModel model;
        int id = row[model.Id];

        if (presenter) {
            presenter->DeleteCustomer(id);
        }
    }
}

void RentCustomerListViewGtk::onAddClick() {
    if (presenter) {
        presenter->AddCustomer();
    }
}

