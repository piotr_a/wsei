//
// Created by Adamczyk Piotr on 10.12.2019.
//

#ifndef WSEICARRENTAL_RENTSUMMARYVIEWGTK_H
#define WSEICARRENTAL_RENTSUMMARYVIEWGTK_H


#include "../../Rent/IRentSummaryView.h"
#include "../../Rent/IRentSummaryPresenter.h"
#include <gtkmm-3.0\gtkmm.h>

class RentSummaryViewGtk : public IRentSummaryView {
    IRentSummaryPresenter *presenter;
    Glib::RefPtr<Gtk::Builder> builder;

    void onRentClick();

    void onCancelClick();

public:
    RentSummaryViewGtk(IRentSummaryPresenter *presenter,
                       const Glib::RefPtr<Gtk::Builder> &builder);

    void UpdateView(const VehicleDto &vehicleDto, const CustomerDto &customerDto) override;

    void clear() override;

};


#endif //WSEICARRENTAL_RENTSUMMARYVIEWGTK_H
