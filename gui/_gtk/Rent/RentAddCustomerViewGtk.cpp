//
// Created by Adamczyk Piotr on 09.12.2019.
//

#include "RentAddCustomerViewGtk.h"
#include "../UtilsGtk.h"

RentAddCustomerViewGtk::RentAddCustomerViewGtk(IRentAddCustomerPresenter *presenter,
                                               const Glib::RefPtr<Gtk::Builder> &builder)
        : presenter(presenter),
          builder(builder) {

    UtilsGtk::setSignal<Gtk::ToolButton>(builder, "AddCustomerAdd", *this,
                                         &RentAddCustomerViewGtk::onAddClick);
    UtilsGtk::setSignal<Gtk::ToolButton>(builder, "AddCustomerCancel", *this,
                                         &RentAddCustomerViewGtk::onCancelClick);
}

std::string RentAddCustomerViewGtk::getName() {
    return UtilsGtk::getText<Gtk::Entry>(builder, "AddCustomerName");
}

std::string RentAddCustomerViewGtk::getSurname() {
    return UtilsGtk::getText<Gtk::Entry>(builder, "AddCustomerSurname");
}

std::string RentAddCustomerViewGtk::getAddress() {
    return UtilsGtk::getText<Gtk::Entry>(builder, "AddCustomerAddress");
}

std::string RentAddCustomerViewGtk::getPhoneNumber() {
    return UtilsGtk::getText<Gtk::Entry>(builder, "AddCustomerPhoneNumber");
}

void RentAddCustomerViewGtk::clear() {
    UtilsGtk::setText<Gtk::Entry>(builder, "AddCustomerName", "");
    UtilsGtk::setText<Gtk::Entry>(builder, "AddCustomerSurname", "");
    UtilsGtk::setText<Gtk::Entry>(builder, "AddCustomerAddress", "");
    UtilsGtk::setText<Gtk::Entry>(builder, "AddCustomerPhoneNumber", "");
}

void RentAddCustomerViewGtk::onAddClick() {
    if (presenter) {
        presenter->AddCustomer();
    }
}

void RentAddCustomerViewGtk::onCancelClick() {
    if (presenter) {
        presenter->Cancel();
    }
}
