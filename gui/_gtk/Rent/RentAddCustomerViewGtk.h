//
// Created by Adamczyk Piotr on 09.12.2019.
//

#ifndef WSEICARRENTAL_RENTADDCUSTOMERVIEWGTK_H
#define WSEICARRENTAL_RENTADDCUSTOMERVIEWGTK_H

#include <gtkmm-3.0\gtkmm.h>
#include <memory>
#include "../../Rent/IRentAddCustomerPresenter.h"
#include "../../Rent/IRentAddCustomerView.h"

class RentAddCustomerViewGtk : public IRentAddCustomerView {
private:
    IRentAddCustomerPresenter *presenter;
    Glib::RefPtr<Gtk::Builder> builder;

    void onAddClick();

    void onCancelClick();

public:
    RentAddCustomerViewGtk(IRentAddCustomerPresenter *presenter,
                           const Glib::RefPtr<Gtk::Builder> &builder);

    std::string getName() override;

    std::string getSurname() override;

    std::string getAddress() override;

    std::string getPhoneNumber() override;

    void clear() override;


};


#endif //WSEICARRENTAL_RENTADDCUSTOMERVIEWGTK_H
