//
// Created by Adamczyk Piotr on 10.12.2019.
//

#include "RentSummaryViewGtk.h"
#include "../UtilsGtk.h"

RentSummaryViewGtk::RentSummaryViewGtk(IRentSummaryPresenter *presenter,
                                       const Glib::RefPtr<Gtk::Builder> &builder)
        : presenter(presenter),
          builder(builder) {


    UtilsGtk::setSignal<Gtk::ToolButton>(builder, "RentalSummaryRent", *this,
                                         &RentSummaryViewGtk::onRentClick);
    UtilsGtk::setSignal<Gtk::ToolButton>(builder, "RentalSummaryCancel", *this,
                                         &RentSummaryViewGtk::onCancelClick);
}

void RentSummaryViewGtk::UpdateView(const VehicleDto &vehicleDto, const CustomerDto &customerDto) {
    UtilsGtk::setText<Gtk::Label>(builder, "RentalSummarySelectedVehicleLabel", vehicleDto.plate);
    UtilsGtk::setText<Gtk::Label>(builder, "RentalSummarySelectedCustomerLabel",
                                  customerDto.Name + " " + customerDto.Surname);
}

void RentSummaryViewGtk::clear() {
    UtilsGtk::setText<Gtk::Label>(builder, "RentalSummarySelectedVehicleLabel", "");
    UtilsGtk::setText<Gtk::Label>(builder, "RentalSummarySelectedCustomerLabel", "");
}

void RentSummaryViewGtk::onRentClick() {
    if (presenter) {
        presenter->Rent();
    }
}

void RentSummaryViewGtk::onCancelClick() {
    if (presenter) {
        presenter->Cancel();
    }
}
