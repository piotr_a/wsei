//
// Created by Adamczyk Piotr on 04.12.2019.
//

#include <gtkmm/liststore.h>
#include "RentVehicleListViewGtk.h"
#include "../UtilsGtk.h"
#include <glib.h>

class RentVehicleListModel : public Gtk::TreeModel::ColumnRecord {
public:
    RentVehicleListModel() {
        // This order must match the column order in the .glade file
        this->add(this->id);
        this->add(this->plate);
        this->add(this->type);
        this->add(this->description);
        this->add(this->price);
    }

    // These types must match those for the model in the .glade file
    Gtk::TreeModelColumn<gint> id;
    Gtk::TreeModelColumn<Glib::ustring> plate;
    Gtk::TreeModelColumn<Glib::ustring> type;
    Gtk::TreeModelColumn<Glib::ustring> description;
    Gtk::TreeModelColumn<Glib::ustring> price;
};


RentVehicleListViewGtk::RentVehicleListViewGtk(IRentVehicleListPresenter *presenter,
                                               const Glib::RefPtr<Gtk::Builder> &builder)
        : presenter(presenter),
          builder(builder) {

    UtilsGtk::setSignal<Gtk::ToolButton>(builder, "RentVehicleListSelect", *this,
                                         &RentVehicleListViewGtk::onSelectClick);
}

void RentVehicleListViewGtk::UpdateVehicles(const std::vector<VehicleDto> &vehicleDtos) {

    auto vehicleStore = Glib::RefPtr<Gtk::ListStore>::cast_dynamic(builder->get_object("RentVehicleListStore"));
    vehicleStore->freeze_notify();
    vehicleStore->clear();

    RentVehicleListModel model;

    for (auto &&vehicleDto : vehicleDtos) {
        auto row = *(vehicleStore->append());
        row[model.id] = vehicleDto.id;
        row[model.plate] = vehicleDto.plate;
        row[model.type] = vehicleDto.type;
        row[model.description] = vehicleDto.description;
        row[model.price] = vehicleDto.price;
    }
    vehicleStore->thaw_notify();
}

void RentVehicleListViewGtk::onSelectClick() {
    auto vehicleList = Glib::RefPtr<Gtk::TreeView>::cast_dynamic(builder->get_object("RentVehicleList"));
    auto vehicleListSelected = vehicleList->get_selection();
    Gtk::TreeModel::Row row = *vehicleListSelected->get_selected();
    if (row) {
        RentVehicleListModel model;
        int id = row[model.id];

        if (presenter) {
            presenter->SelectVehicle(id);
        }
    }
}




