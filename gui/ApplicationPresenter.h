//
// Created by Adamczyk Piotr on 05.12.2019.
//

#ifndef WSEICARRENTAL_APPLICATIONPRESENTER_H
#define WSEICARRENTAL_APPLICATIONPRESENTER_H


#include "IApplicationPresenter.h"
#include "../domain/Application.h"
#include "../domain/Services.h"
#include "Dashboard/DashboardPresenter.h"
#include "Rent/RentPresenter.h"
#include "ReturnVehicle/ReturnVehiclePresenter.h"
#include "Vehicles/VehiclesPresenter.h"
#include "IViewBuilder.h"

class ApplicationPresenter : public IApplicationPresenter {
private:
    std::shared_ptr<IApplicationView> view;
    Application *application;

    std::shared_ptr<DashboardPresenter> dashboardPresenter;
    std::shared_ptr<RentPresenter> rentPresenter;
    std::shared_ptr<ReturnVehiclePresenter> returnVehiclePresenter;
    std::shared_ptr<VehiclesPresenter> vehiclesPresenter;

public:
    explicit ApplicationPresenter(Application *application, Services &services,
                                  std::shared_ptr<IViewBuilder> &viewBuilder);


    void Run();

    void DashboardView() const;

    void RentVehicleView() const;

    void ReturnVehicleView() const;

    void VehiclesView() const;

    void Dashboard() override;

    void RentVehicle() override;

    void ReturnVehicle() override;

    void Vehicles() override;

};


#endif //WSEICARRENTAL_APPLICATIONPRESENTER_H
