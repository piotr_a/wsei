//
// Created by Adamczyk Piotr on 05.12.2019.
//

#ifndef WSEICARRENTAL_IAPPLICATIONVIEW_H
#define WSEICARRENTAL_IAPPLICATIONVIEW_H


class IApplicationView {
public:
    virtual ~IApplicationView() = default;

    virtual void Run() = 0;

    virtual void ShowDashboard() = 0;

    virtual void ShowRentCar() = 0;

    virtual void ShowReturnCar() = 0;

    virtual void ShowVehicles() = 0;
};


#endif //WSEICARRENTAL_IAPPLICATIONVIEW_H
